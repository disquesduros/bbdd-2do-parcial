CREATE DEFINER=`root`@`localhost` PROCEDURE `new_pesaje`(IN id_pesaje VARCHAR(10),IN rp_vaca VARCHAR(3),IN peso float,IN fecha_pesaje timestamp)
BEGIN
insert into pesaje values(id_pesaje,(select rp from animal where rp=rp_vaca),peso,fecha_pesaje);
END