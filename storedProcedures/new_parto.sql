CREATE DEFINER=`root`@`localhost` PROCEDURE `new_parto`(IN rp_padre VARCHAR(10),IN fecha_parto timestamp,IN sexo VARCHAR(10),IN estado VARCHAR(10),IN id_parto VARCHAR(5),IN rp_vaca VARCHAR(3))
BEGIN
insert into partos values((select rp from animal where rp=rp_padre),fecha_parto,sexo,estado,id_parto,(select rp from animal where rp=rp_vaca));
END