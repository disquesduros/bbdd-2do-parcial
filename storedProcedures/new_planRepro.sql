CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planRepro`(IN id_repro VARCHAR(5),IN rp_padre VARCHAR(10),IN rp_vaca VARCHAR(3),IN apli_dispo BIT(1),IN condi_corpo float,in palpacion BIT(1),IN comentario VARCHAR(100),IN tipo_repro VARCHAR(20))
BEGIN
insert into plan_reproductivo values(id_repro,(select rp from animal where rp=rp_padre),(select rp from animal where rp=rp_vaca),apli_dispo,condi_corpo,palpacion,comentario,tipo_repro);
END