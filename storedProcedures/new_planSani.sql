CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli datetime,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
BEGIN
insert into plan_sanitario(id_plan,nombre,tipo,fecha_aplicacion,rp_vaca,nombre_med,dosis)
 values(id_plan,nombre,tipo,fecha_apli,(select rp from animal where rp=rp_vaca),nombre_med,dosis);
END