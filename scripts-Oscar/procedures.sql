
-- -----------------------------------------------------
-- procedure borrar_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_area`(IN nom VARCHAR(30))
BEGIN
Delete From area where nombre=nom;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure borrar_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_persona`(in ced varchar(10))
BEGIN
Delete From persona where cedula=ced;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_animal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal`()
BEGIN
select * from animal;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_area`()
BEGIN
select * from area;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_areaNombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_areaNombre`(IN nom VARCHAR(30))
BEGIN
select * from area where nombre=nom;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_bajas
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_bajas`()
BEGIN
select * from bajas;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_medicamento
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_medicamento`()
BEGIN
SELECT * from medicamento;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_personNombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_personNombre`(IN nom VARCHAR(30))
BEGIN
select * from persona where nombre=nom;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_persona`()
BEGIN
select * from persona;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_sanitario
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_sanitario`()
BEGIN
select plan_sanitario.id_plan, plan_sanitario.nombre, plan_sanitario.fecha_aplicacion, plan_sanitario.tipo,
plan_sanitario.nombre_med, medicamento.posologia, medicamento.periodicidad, plan_sanitario.dosis, pesaje.peso
from medicamento, plan_sanitario, animal, pesaje
where medicamento.nombre = plan_sanitario.nombre_med and animal.RP = plan_sanitario.rp_vaca and animal.RP = pesaje.rp_vaca;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_sanitario_tabla
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_sanitario_tabla`(IN cod INT(11))
BEGIN
select * from plan_sanitario where plan_sanitario.id_plan  = cod ;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_animal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_animal`(IN rp VARCHAR(3),IN n_arete VARCHAR(4),IN id_area VARCHAR(30),IN nom VARCHAR(15),IN sexo VARCHAR(10),IN fecha_llega timestamp,IN haci_ori VARCHAR(15),IN propi VARCHAR(30),IN rp_madre VARCHAR(3),IN rp_padre VARCHAR(3),IN estado VARCHAR(10),IN raza VARCHAR(15))
BEGIN
INSERT INTO animal values(rp,n_arete,(SELECT codigo FROM area  WHERE nombre = id_area),nom,sexo,fecha_llega,haci_ori,(SELECT cedula FROM persona  WHERE nombre = propi),(select a.rp from animal a where a.rp=rp_madre),(select a.rp from animal a where a.rp=rp_padre),estado,raza);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_area`(IN cod INT(11),IN nom VARCHAR(30), IN dir VARCHAR(40),IN ubi VARCHAR(20),IN dim float,IN capMax VARCHAR(11))
BEGIN
INSERT INTO area
VALUES(cod,nom,dir,ubi,dim,capMax);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_medicamento
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_medicamento`(IN nombre VARCHAR(20), IN tipo VARCHAR(10), IN periodicidad VARCHAR(10),IN posologia VARCHAR(20))
BEGIN
INSERT INTO medicamento(nombre,tipo,periodicidad,posologia)
VALUES(nombre,tipo,periodicidad,posologia);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_parto
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_parto`(IN rp_padre VARCHAR(10),IN fecha_parto timestamp,IN sexo VARCHAR(10),IN estado VARCHAR(10),IN id_parto VARCHAR(5),IN rp_vaca VARCHAR(3))
BEGIN
insert into partos values((select rp from animal where rp=rp_padre),fecha_parto,sexo,estado,id_parto,(select rp from animal where rp=rp_vaca));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_persona`(IN ced VARCHAR(10),IN nom VARCHAR(30),IN rol VARCHAR(30),IN dir VARCHAR(20),IN conven VARCHAR(9),IN movil VARCHAR(10), IN id INT(11))
BEGIN
INSERT INTO persona
VALUES(ced,nom,rol,dir,conven,movil, id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_pesaje
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_pesaje`(IN id_pesaje VARCHAR(10),IN rp_vaca VARCHAR(3),IN peso float,IN fecha_pesaje timestamp)
BEGIN
insert into pesaje values(id_pesaje,(select rp from animal where rp=rp_vaca),peso,fecha_pesaje);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_planRepro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planRepro`(IN id_repro VARCHAR(5),IN rp_padre VARCHAR(10),IN rp_vaca VARCHAR(3),IN apli_dispo BIT(1),IN condi_corpo float,in palpacion BIT(1),IN comentario VARCHAR(100),IN tipo_repro VARCHAR(20))
BEGIN
insert into plan_reproductivo values(id_repro,(select rp from animal where rp=rp_padre),(select rp from animal where rp=rp_vaca),apli_dispo,condi_corpo,palpacion,comentario,tipo_repro);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_planSani
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli datetime,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
BEGIN
insert into plan_sanitario(id_plan,nombre,tipo,fecha_aplicacion,rp_vaca,nombre_med,dosis)
 values(id_plan,nombre,tipo,fecha_apli,(select rp from animal where rp=rp_vaca),nombre_med,dosis);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_area`(IN cod INT(11),IN nom VARCHAR(30), IN dir VARCHAR(40),IN ubi VARCHAR(20),IN dim float,IN capMax VARCHAR(11))
BEGIN
update area
set area.nombre = nom, area.direccion = dir, area.ubicacionGeografica = ubi, area.dimension = dim, area.capacidadMaxima = capMax
where area.codigo = cod;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_persona`(IN ced VARCHAR(10),IN nom VARCHAR(30),IN dir VARCHAR(20),IN conven VARCHAR(9),IN movil VARCHAR(10), IN id INT(11))
BEGIN
update persona
set persona.cedula = ced, persona.nombre = nom, persona.direccion = dir, persona.telefono_casa = conven, persona.telefono_movil = movil
where persona.id_persona = id;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_planSani
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli datetime,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
BEGIN
update plan_sanitario
set plan_sanitario.nombre = nombre, plan_sanitario.tipo = tipo, plan_sanitario.fecha_aplicacion = fecha_apli, plan_sanitario.rp_vaca = rp_vaca, plan_sanitario.nombre_med = nombre_med, plan_sanitario.dosis = dosis
where plan_sanitario.id_plan = id_plan;
END$$

DELIMITER ;
