-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema discosdurostest
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `discosdurostest` ;

-- -----------------------------------------------------
-- Schema discosdurostest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `discosdurostest` DEFAULT CHARACTER SET utf8 ;
USE `discosdurostest` ;

-- -----------------------------------------------------
-- Table `discosdurostest`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`persona` (
  `cedula` VARCHAR(10) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `rol` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(20) NOT NULL,
  `telefono_casa` VARCHAR(9) NULL DEFAULT NULL,
  `telefono_movil` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`cedula`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`area`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`area` (
  `codigo` VARCHAR(3) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(40) NOT NULL,
  `ubicacionGeografica` VARCHAR(20) NULL DEFAULT NULL,
  `dimension` FLOAT NOT NULL,
  `capacidadMaxima` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`animal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`animal` (
  `RP` VARCHAR(3) NOT NULL,
  `n_arete` VARCHAR(4) NULL DEFAULT NULL,
  `id_area` VARCHAR(3) NOT NULL,
  `nombre` VARCHAR(15) NULL DEFAULT NULL,
  `sexo` VARCHAR(10) NOT NULL,
  `fecha_llegada` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hacienda_origen` VARCHAR(15) NULL DEFAULT NULL,
  `propietario` VARCHAR(10) NOT NULL,
  `rp_madre` VARCHAR(3) NULL DEFAULT NULL,
  `rp_padre` VARCHAR(3) NULL DEFAULT NULL,
  `estado` VARCHAR(10) NOT NULL,
  `raza` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`RP`),
  INDEX `animal_fk1_idx` (`propietario` ASC),
  INDEX `animal_fk2_idx` (`id_area` ASC),
  INDEX `animal_fk3_idx` (`rp_madre` ASC),
  INDEX `animal_fk4_idx` (`rp_padre` ASC),
  CONSTRAINT `animal_fk1`
    FOREIGN KEY (`propietario`)
    REFERENCES `discosdurostest`.`persona` (`cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `animal_fk2`
    FOREIGN KEY (`id_area`)
    REFERENCES `discosdurostest`.`area` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `animal_fk3`
    FOREIGN KEY (`rp_madre`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `animal_fk4`
    FOREIGN KEY (`rp_padre`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`partos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`partos` (
  `rp_padre` VARCHAR(10) NOT NULL,
  `fecha_parto` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sexo` VARCHAR(10) NOT NULL,
  `estado` VARCHAR(10) NOT NULL,
  `id_parto` VARCHAR(5) NOT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`id_parto`),
  INDEX `fk1_idx` (`rp_padre` ASC),
  INDEX `fk2_idx` (`rp_vaca` ASC),
  CONSTRAINT `partos_fk1`
    FOREIGN KEY (`rp_padre`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `partos_fk2`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`animal_parto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`animal_parto` (
  `RP` VARCHAR(3) NOT NULL,
  `id_parto` VARCHAR(5) NOT NULL,
  INDEX `animal_parto_fk1_idx` (`id_parto` ASC),
  INDEX `animal_parto_fk2_idx` (`RP` ASC),
  CONSTRAINT `animal_parto_fk1`
    FOREIGN KEY (`id_parto`)
    REFERENCES `discosdurostest`.`partos` (`id_parto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `animal_parto_fk2`
    FOREIGN KEY (`RP`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`bajas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`bajas` (
  `id_baja` VARCHAR(3) NOT NULL,
  `fecha_baja` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_baja` VARCHAR(10) NOT NULL,
  `peso_libras` FLOAT NULL DEFAULT NULL,
  `precio_por_libras` FLOAT NULL DEFAULT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `total` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`id_baja`),
  UNIQUE INDEX `id_baja_UNIQUE` (`id_baja` ASC),
  INDEX `bajas_fk1_idx` (`rp_vaca` ASC),
  CONSTRAINT `bajas_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`plan_reproductivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`plan_reproductivo` (
  `id_reproduccion` VARCHAR(5) NOT NULL,
  `rp_padre` VARCHAR(10) NOT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `aplica_dispositivo` BIT(1) NOT NULL,
  `condicion_corporal` FLOAT NOT NULL,
  `palpacion` BIT(1) NOT NULL,
  `comentario` VARCHAR(100) NULL DEFAULT NULL,
  `tipo_reproduccion` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_reproduccion`),
  UNIQUE INDEX `id_reproduccion_UNIQUE` (`id_reproduccion` ASC),
  INDEX `plan_reproductivo_fk1_idx` (`rp_padre` ASC),
  INDEX `plan_reproductivo_fk2_idx` (`rp_vaca` ASC),
  CONSTRAINT `plan_reproductivo_fk1`
    FOREIGN KEY (`rp_padre`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `plan_reproductivo_fk2`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`control_gestacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`control_gestacion` (
  `id_reproduccion` VARCHAR(5) NOT NULL,
  `fecha_monta` VARCHAR(5) NOT NULL COMMENT 'IATF',
  `meses_gestacion` INT(11) NOT NULL,
  `dias_gestacion` INT(11) NOT NULL,
  `fecha_estimada_parto` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX `control_gestacion_fk1_idx` (`id_reproduccion` ASC),
  CONSTRAINT `control_gestacion_fk1`
    FOREIGN KEY (`id_reproduccion`)
    REFERENCES `discosdurostest`.`plan_reproductivo` (`id_reproduccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`control_partos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`control_partos` (
  `rp_animal` VARCHAR(3) NOT NULL,
  `meses_abiertos` INT(11) NOT NULL,
  `dias_abiertos` INT(11) NOT NULL,
  `fecha_ultima_parto` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX `control_partos_fk1_idx` (`rp_animal` ASC),
  CONSTRAINT `control_partos_fk1`
    FOREIGN KEY (`rp_animal`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`gastos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`gastos` (
  `idgastos` VARCHAR(3) NOT NULL,
  `fecha` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_rubro` VARCHAR(10) NULL DEFAULT NULL,
  `detalle` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`idgastos`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`desglose_gastos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`desglose_gastos` (
  `idgastos` VARCHAR(3) NOT NULL,
  `cantidad` INT(11) NOT NULL,
  `costo_unitario` FLOAT NOT NULL,
  `subtotal` FLOAT NOT NULL,
  `descuento` FLOAT NOT NULL,
  `cargos_adicionales` FLOAT NULL DEFAULT NULL,
  `total` FLOAT NOT NULL,
  INDEX `fk1_idx` (`idgastos` ASC),
  CONSTRAINT `fk1`
    FOREIGN KEY (`idgastos`)
    REFERENCES `discosdurostest`.`gastos` (`idgastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`edades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`edades` (
  `RP` VARCHAR(3) NOT NULL,
  `fecha_nacimiento` DATETIME NOT NULL,
  `edades_años` INT(11) NOT NULL,
  `edades_meses` INT(11) NOT NULL,
  `edades_dias` INT(11) NOT NULL,
  `clasificacion` VARCHAR(10) NOT NULL,
  INDEX `edades_fk1_idx` (`RP` ASC),
  CONSTRAINT `edades_fk1`
    FOREIGN KEY (`RP`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`gastopersonal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`gastopersonal` (
  `cedula` VARCHAR(10) NOT NULL,
  `idgastos` VARCHAR(3) NOT NULL,
  INDEX `cedula_idx` (`cedula` ASC),
  INDEX `idgastos_idx` (`idgastos` ASC),
  CONSTRAINT `cedula`
    FOREIGN KEY (`cedula`)
    REFERENCES `discosdurostest`.`persona` (`cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idgastos`
    FOREIGN KEY (`idgastos`)
    REFERENCES `discosdurostest`.`gastos` (`idgastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`ingresos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`ingresos` (
  `id_ingresos` VARCHAR(3) NOT NULL,
  `id_bajas` VARCHAR(3) NOT NULL,
  `detalle` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`id_ingresos`),
  UNIQUE INDEX `id_bajas_UNIQUE` (`id_bajas` ASC),
  UNIQUE INDEX `id_ingresos_UNIQUE` (`id_ingresos` ASC),
  CONSTRAINT `ingresos_fk1`
    FOREIGN KEY (`id_bajas`)
    REFERENCES `discosdurostest`.`bajas` (`id_baja`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`medicamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`medicamento` (
  `nombre` VARCHAR(20) NOT NULL,
  `tipo` VARCHAR(10) NOT NULL,
  `periodicidad` VARCHAR(10) NOT NULL,
  `posologia` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`nombre`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`pesaje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`pesaje` (
  `idpesaje` VARCHAR(10) NOT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `peso` FLOAT NOT NULL,
  `fecha_pesaje` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpesaje`),
  INDEX `pesaje_fk1_idx` (`rp_vaca` ASC),
  CONSTRAINT `pesaje_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`plan_sanitario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`plan_sanitario` (
  `id_plan` INT(11) NOT NULL,
  `nombre` VARCHAR(20) NOT NULL,
  `tipo` VARCHAR(10) NOT NULL,
  `fecha_aplicacion` DATETIME NOT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `nombre_med` VARCHAR(20) NOT NULL,
  `dosis` FLOAT NOT NULL,
  PRIMARY KEY (`id_plan`),
  INDEX `plan_sanitario_fk1_idx` (`rp_vaca` ASC),
  INDEX `plan_sanitario_fk2_idx` (`nombre_med` ASC),
  CONSTRAINT `plan_sanitario_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `plan_sanitario_fk2`
    FOREIGN KEY (`nombre_med`)
    REFERENCES `discosdurostest`.`medicamento` (`nombre`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `discosdurostest` ;

-- -----------------------------------------------------
-- procedure borrar_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_area`(IN nom VARCHAR(30))
BEGIN
Delete From area where nombre=nom;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure borrar_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_persona`(in ced varchar(10))
BEGIN
Delete From persona where cedula=ced;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_animal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal`()
BEGIN
select * from animal;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_area`()
BEGIN
select * from area;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_areaNombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_areaNombre`(IN nom VARCHAR(30))
BEGIN
select * from area where nombre=nom;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_medicamento
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_medicamento`()
BEGIN
SELECT * from medicamento;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_personNombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_personNombre`(IN nom VARCHAR(30))
BEGIN
select * from persona where nombre=nom;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_persona`()
BEGIN
select * from persona;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_animal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_animal`(IN rp VARCHAR(3),IN n_arete VARCHAR(4),IN id_area VARCHAR(30),IN nom VARCHAR(15),IN sexo VARCHAR(10),IN fecha_llega timestamp,IN haci_ori VARCHAR(15),IN propi VARCHAR(30),IN rp_madre VARCHAR(3),IN rp_padre VARCHAR(3),IN estado VARCHAR(10),IN raza VARCHAR(15))
BEGIN
INSERT INTO animal values(rp,n_arete,(SELECT codigo FROM area  WHERE nombre = id_area),nom,sexo,fecha_llega,haci_ori,(SELECT cedula FROM persona  WHERE nombre = propi),(select a.rp from animal a where a.rp=rp_madre),(select a.rp from animal a where a.rp=rp_padre),estado,raza);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_area`(IN cod VARCHAR(3), IN nom VARCHAR(30), IN dir VARCHAR(40),IN ubi VARCHAR(20),IN dim float,IN capMax VARCHAR(11))
BEGIN
INSERT INTO area
VALUES(cod,nom,dir,ubi,dim,capMax);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_medicamento
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_medicamento`(IN nombre VARCHAR(20), IN tipo VARCHAR(10), IN periodicidad VARCHAR(10),IN posologia VARCHAR(20))
BEGIN
INSERT INTO medicamento(nombre,tipo,periodicidad,posologia)
VALUES(nombre,tipo,periodicidad,posologia);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_parto
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_parto`(IN rp_padre VARCHAR(10),IN fecha_parto timestamp,IN sexo VARCHAR(10),IN estado VARCHAR(10),IN id_parto VARCHAR(5),IN rp_vaca VARCHAR(3))
BEGIN
insert into partos values((select rp from animal where rp=rp_padre),fecha_parto,sexo,estado,id_parto,(select rp from animal where rp=rp_vaca));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_persona`(IN ced VARCHAR(10),IN nom VARCHAR(30),IN rol VARCHAR(30),IN dir VARCHAR(20),IN conven VARCHAR(9),IN movil VARCHAR(10))
BEGIN
INSERT INTO persona
VALUES(ced,nom,rol,dir,conven,movil);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_pesaje
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_pesaje`(IN id_pesaje VARCHAR(10),IN rp_vaca VARCHAR(3),IN peso float,IN fecha_pesaje timestamp)
BEGIN
insert into pesaje values(id_pesaje,(select rp from animal where rp=rp_vaca),peso,fecha_pesaje);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_planRepro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planRepro`(IN id_repro VARCHAR(5),IN rp_padre VARCHAR(10),IN rp_vaca VARCHAR(3),IN apli_dispo BIT(1),IN condi_corpo float,in palpacion BIT(1),IN comentario VARCHAR(100),IN tipo_repro VARCHAR(20))
BEGIN
insert into plan_reproductivo values(id_repro,(select rp from animal where rp=rp_padre),(select rp from animal where rp=rp_vaca),apli_dispo,condi_corpo,palpacion,comentario,tipo_repro);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_planSani
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli datetime,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
BEGIN
insert into plan_sanitario(id_plan,nombre,tipo,fecha_aplicacion,rp_vaca,nombre_med,dosis)
 values(id_plan,nombre,tipo,fecha_apli,(select rp from animal where rp=rp_vaca),nombre_med,dosis);
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
