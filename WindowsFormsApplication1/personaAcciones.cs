﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class personaAcciones
    {
        public static int Agregar(persona persona)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_persona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("ced", persona.ced));
            cmd.Parameters.Add(new MySqlParameter("nom", persona.nombre));
            cmd.Parameters.Add(new MySqlParameter("rol", persona.rol));
            cmd.Parameters.Add(new MySqlParameter("dir", persona.dir));
            cmd.Parameters.Add(new MySqlParameter("conven", persona.conven));
            cmd.Parameters.Add(new MySqlParameter("movil",persona.movil));
            cmd.Parameters.Add(new MySqlParameter("id", null));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }
        public static int Eliminar(string ced)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_persona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("ced",ced));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;

        }

        public static int Actualizar(persona persona)
        {
            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("update_persona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("ced", persona.ced));
            cmd.Parameters.Add(new MySqlParameter("nom", persona.nombre));
            cmd.Parameters.Add(new MySqlParameter("rol", persona.rol));
            cmd.Parameters.Add(new MySqlParameter("dir", persona.dir));
            cmd.Parameters.Add(new MySqlParameter("conven", persona.conven));
            cmd.Parameters.Add(new MySqlParameter("movil", persona.movil));
            cmd.Parameters.Add(new MySqlParameter("id", persona.cod));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }
    }
}
