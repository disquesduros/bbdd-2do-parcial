﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class ConsultaInfoPlanSanitario : Form
    {
        public ConsultaInfoPlanSanitario()
        {
            InitializeComponent();
        }
        
        public IngresoPlanSanitario plan = new IngresoPlanSanitario(false);
        

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //WindowsFormsApplication1.IngresoPlanSanitario plan = new WindowsFormsApplication1.IngresoPlanSanitario();
            plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            plan.Show();
            this.Hide();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                 IngresoPlanSanitario plan = new IngresoPlanSanitario(true);
               // WindowsFormsApplication1.IngresoPlanSanitario plan = new WindowsFormsApplication1.IngresoPlanSanitario(true);

                string celda = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_plan_sanitario_tabla", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("cod", celda));

                
                plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                plan.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
               // planSani.fecha_apli = dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
                
                plan.txtID.Text = reader.GetString(0);
                plan.txtnombre.Text = reader.GetString(1);
                plan.cmbtipo.Text = reader.GetString(2);
                plan.dtfecha.Text = reader.GetString(3);
                plan.txtrp.Text = reader.GetString(4);
                plan.cmbmedicamento.Text = reader.GetString(5);
                plan.txtdosis.Text = reader.GetString(6);

                this.Hide();

                
                //WindowsFormsApplication1.IngresoPlanSanitario plan = new WindowsFormsApplication1.IngresoPlanSanitario();
               /* plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                plan.Show();
                this.Hide();*/
            }
        }

<<<<<<< HEAD
        private void ConsultaInfoPlanSanitario_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_plan_sanitario", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dataGridView1.Rows.Add(true, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7), reader.GetString(8));
=======
        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                string idPlanSani = Convert.ToString(dataGridView1.CurrentRow.Cells[1].Value);
                planSaniAcciones.Eliminar(idPlanSani);
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);

            }
            else
                MessageBox.Show("Debe seleccionar una fila");
>>>>>>> 10e00d31371f88936caa9dde52cdef3592be6b43
        }
    }
}
