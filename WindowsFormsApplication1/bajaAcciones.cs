﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class bajaAcciones
    {
        public static int Agregar(baja baja)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_baja", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add(new MySqlParameter("idbaja", baja.idbaja.));
            cmd.Parameters.Add(new MySqlParameter("fecha", baja.fecha));
            cmd.Parameters.Add(new MySqlParameter("tipo", baja.tipo));
            cmd.Parameters.Add(new MySqlParameter("peso", baja.peso));
            cmd.Parameters.Add(new MySqlParameter("precio", baja.preciolb));
            cmd.Parameters.Add(new MySqlParameter("rp", baja.rpvaca));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string id)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_baja", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("idbaja", id));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }

    }
}

