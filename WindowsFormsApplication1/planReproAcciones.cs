﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class planReproAcciones
    {
        public static int Agregar(planRepro planRepro)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_planRepro", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("id_repro",planRepro.id_repro));
            cmd.Parameters.Add(new MySqlParameter("rp_padre", planRepro.rp_padre));
            cmd.Parameters.Add(new MySqlParameter("rp_vaca", planRepro.rp_vaca));
            cmd.Parameters.Add(new MySqlParameter("apli_dispo", planRepro.apli_dispo));
            cmd.Parameters.Add(new MySqlParameter("condi_corpo", planRepro.condi_corpo));
            cmd.Parameters.Add(new MySqlParameter("palpacion", planRepro.palpacion));
            cmd.Parameters.Add(new MySqlParameter("comentario", planRepro.comentario));
            cmd.Parameters.Add(new MySqlParameter("tipo_repro", planRepro.tipo_repro));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string idPlan) 
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_planRepro", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("idPlan", idPlan));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }

    }
}
