﻿namespace WindowsFormsApplication1
{
    partial class IngresoAnimalNuevo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRP = new System.Windows.Forms.Label();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.cmbUbicacion = new System.Windows.Forms.ComboBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblArete = new System.Windows.Forms.Label();
            this.txtArete = new System.Windows.Forms.TextBox();
            this.lblSexo = new System.Windows.Forms.Label();
            this.cmbSexo = new System.Windows.Forms.ComboBox();
            this.lblEstado = new System.Windows.Forms.Label();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.lblRaza = new System.Windows.Forms.Label();
            this.lblRGD = new System.Windows.Forms.Label();
            this.txtRGD = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbPropietario = new System.Windows.Forms.ComboBox();
            this.lblRPPadre = new System.Windows.Forms.Label();
            this.txtRPPadre = new System.Windows.Forms.TextBox();
            this.lblRPMadre = new System.Windows.Forms.Label();
            this.lblRGDPadre = new System.Windows.Forms.Label();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.lblFechaLlegada = new System.Windows.Forms.Label();
            this.lblHaciendaOrigen = new System.Windows.Forms.Label();
            this.txtRPMadre = new System.Windows.Forms.TextBox();
            this.txtRGDPadre = new System.Windows.Forms.TextBox();
            this.txtHaciendaOrigen = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaLlegada = new System.Windows.Forms.DateTimePicker();
            this.cmbRaza = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.Location = new System.Drawing.Point(70, 47);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(25, 13);
            this.lblRP.TabIndex = 0;
            this.lblRP.Text = "RP:";
            // 
            // txtRP
            // 
            this.txtRP.Location = new System.Drawing.Point(137, 40);
            this.txtRP.MaxLength = 3;
            this.txtRP.Name = "txtRP";
            this.txtRP.Size = new System.Drawing.Size(121, 20);
            this.txtRP.TabIndex = 1;
            this.txtRP.TextChanged += new System.EventHandler(this.txtRP_TextChanged);
            this.txtRP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRP_KeyPress);
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.AutoSize = true;
            this.lblUbicacion.Location = new System.Drawing.Point(346, 47);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(58, 13);
            this.lblUbicacion.TabIndex = 2;
            this.lblUbicacion.Text = "Ubicación:";
            // 
            // cmbUbicacion
            // 
            this.cmbUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUbicacion.FormattingEnabled = true;
            this.cmbUbicacion.Items.AddRange(new object[] {
            "LA LAGUNA",
            "SAN FERNANDO",
            "SONIA MARIA"});
            this.cmbUbicacion.Location = new System.Drawing.Point(413, 40);
            this.cmbUbicacion.Name = "cmbUbicacion";
            this.cmbUbicacion.Size = new System.Drawing.Size(121, 21);
            this.cmbUbicacion.TabIndex = 3;
            this.cmbUbicacion.SelectedIndexChanged += new System.EventHandler(this.cmbUbicacion_SelectedIndexChanged);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(70, 81);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 4;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(137, 74);
            this.txtNombre.MaxLength = 15;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(121, 20);
            this.txtNombre.TabIndex = 5;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // lblArete
            // 
            this.lblArete.AutoSize = true;
            this.lblArete.Location = new System.Drawing.Point(346, 81);
            this.lblArete.Name = "lblArete";
            this.lblArete.Size = new System.Drawing.Size(55, 13);
            this.lblArete.TabIndex = 6;
            this.lblArete.Text = "No. Arete:";
            // 
            // txtArete
            // 
            this.txtArete.Location = new System.Drawing.Point(413, 74);
            this.txtArete.MaxLength = 4;
            this.txtArete.Name = "txtArete";
            this.txtArete.Size = new System.Drawing.Size(119, 20);
            this.txtArete.TabIndex = 7;
            this.txtArete.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtArete_KeyPress);
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Location = new System.Drawing.Point(70, 112);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(34, 13);
            this.lblSexo.TabIndex = 8;
            this.lblSexo.Text = "Sexo:";
            // 
            // cmbSexo
            // 
            this.cmbSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSexo.FormattingEnabled = true;
            this.cmbSexo.Items.AddRange(new object[] {
            "HEMBRA",
            "MACHO"});
            this.cmbSexo.Location = new System.Drawing.Point(137, 104);
            this.cmbSexo.Name = "cmbSexo";
            this.cmbSexo.Size = new System.Drawing.Size(121, 21);
            this.cmbSexo.TabIndex = 9;
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(346, 112);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(43, 13);
            this.lblEstado.TabIndex = 10;
            this.lblEstado.Text = "Estado:";
            // 
            // cmbEstado
            // 
            this.cmbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Items.AddRange(new object[] {
            "ACTIVO",
            "INACTIVO"});
            this.cmbEstado.Location = new System.Drawing.Point(413, 104);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(121, 21);
            this.cmbEstado.TabIndex = 11;
            // 
            // lblRaza
            // 
            this.lblRaza.AutoSize = true;
            this.lblRaza.Location = new System.Drawing.Point(70, 140);
            this.lblRaza.Name = "lblRaza";
            this.lblRaza.Size = new System.Drawing.Size(35, 13);
            this.lblRaza.TabIndex = 12;
            this.lblRaza.Text = "Raza:";
            // 
            // lblRGD
            // 
            this.lblRGD.AutoSize = true;
            this.lblRGD.Location = new System.Drawing.Point(346, 140);
            this.lblRGD.Name = "lblRGD";
            this.lblRGD.Size = new System.Drawing.Size(34, 13);
            this.lblRGD.TabIndex = 14;
            this.lblRGD.Text = "RGD:";
            // 
            // txtRGD
            // 
            this.txtRGD.Location = new System.Drawing.Point(413, 137);
            this.txtRGD.MaxLength = 10;
            this.txtRGD.Name = "txtRGD";
            this.txtRGD.Size = new System.Drawing.Size(119, 20);
            this.txtRGD.TabIndex = 15;
            this.txtRGD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRGD_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Propietario:";
            // 
            // cmbPropietario
            // 
            this.cmbPropietario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPropietario.FormattingEnabled = true;
            this.cmbPropietario.Location = new System.Drawing.Point(137, 162);
            this.cmbPropietario.Name = "cmbPropietario";
            this.cmbPropietario.Size = new System.Drawing.Size(121, 21);
            this.cmbPropietario.TabIndex = 17;
            this.cmbPropietario.SelectedIndexChanged += new System.EventHandler(this.cmbPropietario_SelectedIndexChanged);
            // 
            // lblRPPadre
            // 
            this.lblRPPadre.AutoSize = true;
            this.lblRPPadre.Location = new System.Drawing.Point(346, 165);
            this.lblRPPadre.Name = "lblRPPadre";
            this.lblRPPadre.Size = new System.Drawing.Size(56, 13);
            this.lblRPPadre.TabIndex = 18;
            this.lblRPPadre.Text = "RP Padre:";
            // 
            // txtRPPadre
            // 
            this.txtRPPadre.Location = new System.Drawing.Point(413, 162);
            this.txtRPPadre.MaxLength = 3;
            this.txtRPPadre.Name = "txtRPPadre";
            this.txtRPPadre.Size = new System.Drawing.Size(119, 20);
            this.txtRPPadre.TabIndex = 19;
            this.txtRPPadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRPPadre_KeyPress);
            // 
            // lblRPMadre
            // 
            this.lblRPMadre.AutoSize = true;
            this.lblRPMadre.Location = new System.Drawing.Point(70, 194);
            this.lblRPMadre.Name = "lblRPMadre";
            this.lblRPMadre.Size = new System.Drawing.Size(58, 13);
            this.lblRPMadre.TabIndex = 20;
            this.lblRPMadre.Text = "RP Madre:";
            // 
            // lblRGDPadre
            // 
            this.lblRGDPadre.AutoSize = true;
            this.lblRGDPadre.Location = new System.Drawing.Point(346, 194);
            this.lblRGDPadre.Name = "lblRGDPadre";
            this.lblRGDPadre.Size = new System.Drawing.Size(65, 13);
            this.lblRGDPadre.TabIndex = 21;
            this.lblRGDPadre.Text = "RGD Padre:";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(70, 229);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(111, 13);
            this.lblFechaNacimiento.TabIndex = 22;
            this.lblFechaNacimiento.Text = "Fecha de Nacimiento:";
            // 
            // lblFechaLlegada
            // 
            this.lblFechaLlegada.AutoSize = true;
            this.lblFechaLlegada.Location = new System.Drawing.Point(70, 260);
            this.lblFechaLlegada.Name = "lblFechaLlegada";
            this.lblFechaLlegada.Size = new System.Drawing.Size(99, 13);
            this.lblFechaLlegada.TabIndex = 23;
            this.lblFechaLlegada.Text = "Fecha de Llegada: ";
            // 
            // lblHaciendaOrigen
            // 
            this.lblHaciendaOrigen.AutoSize = true;
            this.lblHaciendaOrigen.Location = new System.Drawing.Point(70, 288);
            this.lblHaciendaOrigen.Name = "lblHaciendaOrigen";
            this.lblHaciendaOrigen.Size = new System.Drawing.Size(90, 13);
            this.lblHaciendaOrigen.TabIndex = 24;
            this.lblHaciendaOrigen.Text = "Hacienda Origen:";
            // 
            // txtRPMadre
            // 
            this.txtRPMadre.Location = new System.Drawing.Point(137, 190);
            this.txtRPMadre.MaxLength = 3;
            this.txtRPMadre.Name = "txtRPMadre";
            this.txtRPMadre.Size = new System.Drawing.Size(121, 20);
            this.txtRPMadre.TabIndex = 25;
            this.txtRPMadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRPMadre_KeyPress);
            // 
            // txtRGDPadre
            // 
            this.txtRGDPadre.Location = new System.Drawing.Point(413, 189);
            this.txtRGDPadre.MaxLength = 10;
            this.txtRGDPadre.Name = "txtRGDPadre";
            this.txtRGDPadre.Size = new System.Drawing.Size(119, 20);
            this.txtRGDPadre.TabIndex = 26;
            this.txtRGDPadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRGDPadre_KeyPress);
            // 
            // txtHaciendaOrigen
            // 
            this.txtHaciendaOrigen.Location = new System.Drawing.Point(187, 285);
            this.txtHaciendaOrigen.MaxLength = 15;
            this.txtHaciendaOrigen.Name = "txtHaciendaOrigen";
            this.txtHaciendaOrigen.Size = new System.Drawing.Size(200, 20);
            this.txtHaciendaOrigen.TabIndex = 27;
            this.txtHaciendaOrigen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHaciendaOrigen_KeyPress);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(197, 333);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(127, 37);
            this.btnGuardar.TabIndex = 28;
            this.btnGuardar.Text = "GUARDAR/ CERRAR";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnDescartar
            // 
            this.btnDescartar.Location = new System.Drawing.Point(392, 333);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(127, 37);
            this.btnDescartar.TabIndex = 29;
            this.btnDescartar.Text = "DESCARTAR";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(187, 221);
            this.dtpFechaNacimiento.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpFechaNacimiento.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaNacimiento.TabIndex = 30;
            this.dtpFechaNacimiento.Value = new System.DateTime(2015, 12, 20, 17, 8, 33, 0);
            // 
            // dtpFechaLlegada
            // 
            this.dtpFechaLlegada.Location = new System.Drawing.Point(187, 252);
            this.dtpFechaLlegada.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpFechaLlegada.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpFechaLlegada.Name = "dtpFechaLlegada";
            this.dtpFechaLlegada.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaLlegada.TabIndex = 31;
            this.dtpFechaLlegada.Value = new System.DateTime(2015, 12, 20, 17, 8, 42, 0);
            // 
            // cmbRaza
            // 
            this.cmbRaza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRaza.FormattingEnabled = true;
            this.cmbRaza.Items.AddRange(new object[] {
            "NELORE"});
            this.cmbRaza.Location = new System.Drawing.Point(137, 132);
            this.cmbRaza.Name = "cmbRaza";
            this.cmbRaza.Size = new System.Drawing.Size(121, 21);
            this.cmbRaza.TabIndex = 33;
            // 
            // IngresoAnimalNuevo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 397);
            this.Controls.Add(this.cmbRaza);
            this.Controls.Add(this.dtpFechaLlegada);
            this.Controls.Add(this.dtpFechaNacimiento);
            this.Controls.Add(this.btnDescartar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtHaciendaOrigen);
            this.Controls.Add(this.txtRGDPadre);
            this.Controls.Add(this.txtRPMadre);
            this.Controls.Add(this.lblHaciendaOrigen);
            this.Controls.Add(this.lblFechaLlegada);
            this.Controls.Add(this.lblFechaNacimiento);
            this.Controls.Add(this.lblRGDPadre);
            this.Controls.Add(this.lblRPMadre);
            this.Controls.Add(this.txtRPPadre);
            this.Controls.Add(this.lblRPPadre);
            this.Controls.Add(this.cmbPropietario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRGD);
            this.Controls.Add(this.lblRGD);
            this.Controls.Add(this.lblRaza);
            this.Controls.Add(this.cmbEstado);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.cmbSexo);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.txtArete);
            this.Controls.Add(this.lblArete);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.cmbUbicacion);
            this.Controls.Add(this.lblUbicacion);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.lblRP);
            this.Name = "IngresoAnimalNuevo";
            this.Text = "Ingreso Información Animal Nuevo";
            this.Load += new System.EventHandler(this.IngresoAnimalNuevo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRP;
        private System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.ComboBox cmbUbicacion;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblArete;
        private System.Windows.Forms.TextBox txtArete;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.ComboBox cmbSexo;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.ComboBox cmbEstado;
        private System.Windows.Forms.Label lblRaza;
        private System.Windows.Forms.Label lblRGD;
        private System.Windows.Forms.TextBox txtRGD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbPropietario;
        private System.Windows.Forms.Label lblRPPadre;
        private System.Windows.Forms.TextBox txtRPPadre;
        private System.Windows.Forms.Label lblRPMadre;
        private System.Windows.Forms.Label lblRGDPadre;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.Label lblFechaLlegada;
        private System.Windows.Forms.Label lblHaciendaOrigen;
        private System.Windows.Forms.TextBox txtRPMadre;
        private System.Windows.Forms.TextBox txtRGDPadre;
        private System.Windows.Forms.TextBox txtHaciendaOrigen;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnDescartar;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.DateTimePicker dtpFechaLlegada;
        private System.Windows.Forms.ComboBox cmbRaza;
    }
}