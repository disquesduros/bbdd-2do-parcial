﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void btnDatosHacienda_Click(object sender, EventArgs e)
        {
            ConsultaInfoHacienda info = new ConsultaInfoHacienda();
            info.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            info.Show();
            this.Hide();
        }

        private void btnAnimales_Click(object sender, EventArgs e)
        {
            WindowsFormsApplication1.BusquedaAnimales animales= new WindowsFormsApplication1.BusquedaAnimales();
            animales.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            animales.Show();
            this.Hide();
        }

        private void btnBajas_Click(object sender, EventArgs e)
        {
            InformacionBajas bajas = new InformacionBajas();
            bajas.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            bajas.Show();
            this.Hide();
        }

        private void btnGastosIngresos_Click(object sender, EventArgs e)
        {
            ConsultaGastosIngresos gastosing = new ConsultaGastosIngresos();
            gastosing.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            gastosing.Show();
            this.Hide();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            GeneracionReportes reportes = new GeneracionReportes();
            reportes.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            reportes.Show();
            this.Hide();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void btnComprobar_Click(object sender, EventArgs e)
        {
            coneccionBase.ObtenerConexion();
            MessageBox.Show("Conectado");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int contador = 0;
            List<String> _lista = new List<String>();
            MySqlCommand _comando = new MySqlCommand(String.Format(
         "SHOW FULL TABLES FROM discosduros"), coneccionBase.ObtenerConexion());

            MySqlDataReader _reader = _comando.ExecuteReader();
            while (_reader.Read())
            {
                contador++;
                _lista.Add(_reader.GetString(0));
                
            }
            comboBox1.DataSource = _lista;
            label2.Text = contador.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
