﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ConsultaGastosIngresos : Form
    {
        public ConsultaGastosIngresos()
        {
            InitializeComponent();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoGastos gasto = new IngresoGastos();
            gasto.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            gasto.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                IngresoGastos animal = new IngresoGastos();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }else if (e.ColumnIndex == 2)
            {
                InformacionPersonal animal = new InformacionPersonal();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }
        }

        private void ConsultaGastosIngresos_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 1)
            {
                string id_Gastos = Convert.ToString(dataGridView2.CurrentRow.Cells[1].Value);
                gastosAcciones.EliminarGastos(id_Gastos);
                dataGridView2.Rows.Remove(dataGridView2.CurrentRow);

                string id_desgloGastos = Convert.ToString(dataGridView2.CurrentRow.Cells[1].Value);
                gastosAcciones.EliminarDesgloseGastos(id_desgloGastos);
                dataGridView2.Rows.Remove(dataGridView2.CurrentRow);

                //POSIBLE ERROR PORQUE LOS EVENTOS DE ELIMINACION SE TRATAN DE FORMA SEPARADA
                //EN UNA MISMA TABLA, POSIBLE SOLUCION UNIR LAS TABLAS Y APLICAR DE NUEVO ELIMINACION

            }
            else
                MessageBox.Show("Debe seleccionar una fila");
        }
    }
}
