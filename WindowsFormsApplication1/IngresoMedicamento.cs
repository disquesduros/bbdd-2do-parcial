﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoMedicamento : Form
    {
        public IngresoMedicamento()
        {
            InitializeComponent();
        }
        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else

            {
                medicamento medicamento = new medicamento();
                medicamento.nombre = txtNombre.Text.Trim();
                medicamento.tipo = cmbTipo.Text.Trim();
                medicamento.periodicidad = txtPeriodicidad.Text.Trim();
                medicamento.posologia = txtPosologia.Text.Trim();
                int resultado = medicamentoAcciones.Agregar(medicamento);
                if (resultado > 0)
                {
                    MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void IngresoMedicamento_Load(object sender, EventArgs e)
        {

        }
    }
}
