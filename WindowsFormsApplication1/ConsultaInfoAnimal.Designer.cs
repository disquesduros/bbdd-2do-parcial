﻿namespace WindowsFormsApplication1
{
    partial class ConsultaInfoAnimal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDatosGenerales = new System.Windows.Forms.Label();
            this.lnkEditarDatos = new System.Windows.Forms.LinkLabel();
            this.lblRP = new System.Windows.Forms.Label();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblArete = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblRGD = new System.Windows.Forms.Label();
            this.lblRaza = new System.Windows.Forms.Label();
            this.lblPropietario = new System.Windows.Forms.Label();
            this.lblClasificacion = new System.Windows.Forms.Label();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.lblFechaLlegada = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblHaciendaOrigen = new System.Windows.Forms.Label();
            this.lblGenealogia = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Relacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Propietario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nHijos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblControlPartos = new System.Windows.Forms.Label();
            this.lblUltParto = new System.Windows.Forms.Label();
            this.lblMesesAbiertos = new System.Windows.Forms.Label();
            this.lblDiasAbiertos = new System.Windows.Forms.Label();
            this.btnVolver = new System.Windows.Forms.Button();
            this.txtUbicacion = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtArete = new System.Windows.Forms.TextBox();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.txtRGD = new System.Windows.Forms.TextBox();
            this.txtRaza = new System.Windows.Forms.TextBox();
            this.txtPropietario = new System.Windows.Forms.TextBox();
            this.txtClasificacion = new System.Windows.Forms.TextBox();
            this.txtFechaNacimiento = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.txtFechaLlegada = new System.Windows.Forms.TextBox();
            this.txtHaciendaOrigen = new System.Windows.Forms.TextBox();
            this.txtFechaUltParto = new System.Windows.Forms.TextBox();
            this.txtMesesAbiertos = new System.Windows.Forms.TextBox();
            this.txtDiasAbiertos = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDatosGenerales
            // 
            this.lblDatosGenerales.AutoSize = true;
            this.lblDatosGenerales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatosGenerales.Location = new System.Drawing.Point(13, 23);
            this.lblDatosGenerales.Name = "lblDatosGenerales";
            this.lblDatosGenerales.Size = new System.Drawing.Size(114, 15);
            this.lblDatosGenerales.TabIndex = 0;
            this.lblDatosGenerales.Text = "Datos Generales";
            // 
            // lnkEditarDatos
            // 
            this.lnkEditarDatos.AutoSize = true;
            this.lnkEditarDatos.Location = new System.Drawing.Point(163, 25);
            this.lnkEditarDatos.Name = "lnkEditarDatos";
            this.lnkEditarDatos.Size = new System.Drawing.Size(34, 13);
            this.lnkEditarDatos.TabIndex = 1;
            this.lnkEditarDatos.TabStop = true;
            this.lnkEditarDatos.Text = "Editar";
            this.lnkEditarDatos.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEditarDatos_LinkClicked);
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.Location = new System.Drawing.Point(63, 61);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(28, 13);
            this.lblRP.TabIndex = 2;
            this.lblRP.Text = "RP: ";
            // 
            // txtRP
            // 
            this.txtRP.Enabled = false;
            this.txtRP.Location = new System.Drawing.Point(130, 58);
            this.txtRP.MaxLength = 3;
            this.txtRP.Name = "txtRP";
            this.txtRP.ReadOnly = true;
            this.txtRP.Size = new System.Drawing.Size(122, 20);
            this.txtRP.TabIndex = 3;
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.AutoSize = true;
            this.lblUbicacion.Location = new System.Drawing.Point(334, 61);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(58, 13);
            this.lblUbicacion.TabIndex = 4;
            this.lblUbicacion.Text = "Ubicación:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(63, 96);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 5;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblArete
            // 
            this.lblArete.AutoSize = true;
            this.lblArete.Location = new System.Drawing.Point(334, 96);
            this.lblArete.Name = "lblArete";
            this.lblArete.Size = new System.Drawing.Size(55, 13);
            this.lblArete.TabIndex = 6;
            this.lblArete.Text = "No. Arete:";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Location = new System.Drawing.Point(63, 132);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(34, 13);
            this.lblSexo.TabIndex = 7;
            this.lblSexo.Text = "Sexo:";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(334, 132);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(40, 13);
            this.lblEstado.TabIndex = 8;
            this.lblEstado.Text = "Estado";
            // 
            // lblRGD
            // 
            this.lblRGD.AutoSize = true;
            this.lblRGD.Location = new System.Drawing.Point(63, 169);
            this.lblRGD.Name = "lblRGD";
            this.lblRGD.Size = new System.Drawing.Size(34, 13);
            this.lblRGD.TabIndex = 9;
            this.lblRGD.Text = "RGD:";
            // 
            // lblRaza
            // 
            this.lblRaza.AutoSize = true;
            this.lblRaza.Location = new System.Drawing.Point(334, 169);
            this.lblRaza.Name = "lblRaza";
            this.lblRaza.Size = new System.Drawing.Size(35, 13);
            this.lblRaza.TabIndex = 10;
            this.lblRaza.Text = "Raza:";
            // 
            // lblPropietario
            // 
            this.lblPropietario.AutoSize = true;
            this.lblPropietario.Location = new System.Drawing.Point(63, 203);
            this.lblPropietario.Name = "lblPropietario";
            this.lblPropietario.Size = new System.Drawing.Size(60, 13);
            this.lblPropietario.TabIndex = 11;
            this.lblPropietario.Text = "Propietario:";
            // 
            // lblClasificacion
            // 
            this.lblClasificacion.AutoSize = true;
            this.lblClasificacion.Location = new System.Drawing.Point(334, 203);
            this.lblClasificacion.Name = "lblClasificacion";
            this.lblClasificacion.Size = new System.Drawing.Size(72, 13);
            this.lblClasificacion.TabIndex = 12;
            this.lblClasificacion.Text = "Clasificación: ";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(63, 238);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(114, 13);
            this.lblFechaNacimiento.TabIndex = 13;
            this.lblFechaNacimiento.Text = "Fecha de Nacimiento: ";
            // 
            // lblFechaLlegada
            // 
            this.lblFechaLlegada.AutoSize = true;
            this.lblFechaLlegada.Location = new System.Drawing.Point(63, 272);
            this.lblFechaLlegada.Name = "lblFechaLlegada";
            this.lblFechaLlegada.Size = new System.Drawing.Size(96, 13);
            this.lblFechaLlegada.TabIndex = 14;
            this.lblFechaLlegada.Text = "Fecha de Llegada:";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Location = new System.Drawing.Point(334, 238);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(35, 13);
            this.lblEdad.TabIndex = 15;
            this.lblEdad.Text = "Edad:";
            // 
            // lblHaciendaOrigen
            // 
            this.lblHaciendaOrigen.AutoSize = true;
            this.lblHaciendaOrigen.Location = new System.Drawing.Point(334, 272);
            this.lblHaciendaOrigen.Name = "lblHaciendaOrigen";
            this.lblHaciendaOrigen.Size = new System.Drawing.Size(90, 13);
            this.lblHaciendaOrigen.TabIndex = 16;
            this.lblHaciendaOrigen.Text = "Hacienda Origen:";
            // 
            // lblGenealogia
            // 
            this.lblGenealogia.AutoSize = true;
            this.lblGenealogia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenealogia.Location = new System.Drawing.Point(13, 309);
            this.lblGenealogia.Name = "lblGenealogia";
            this.lblGenealogia.Size = new System.Drawing.Size(81, 15);
            this.lblGenealogia.TabIndex = 17;
            this.lblGenealogia.Text = "Genealogía";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RP,
            this.Relacion,
            this.Propietario,
            this.Nombre,
            this.nHijos,
            this.Estado});
            this.dataGridView1.Location = new System.Drawing.Point(46, 338);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(605, 125);
            this.dataGridView1.TabIndex = 18;
            // 
            // RP
            // 
            this.RP.HeaderText = "RP";
            this.RP.Name = "RP";
            this.RP.ReadOnly = true;
            // 
            // Relacion
            // 
            this.Relacion.HeaderText = "Relación ";
            this.Relacion.Name = "Relacion";
            this.Relacion.ReadOnly = true;
            // 
            // Propietario
            // 
            this.Propietario.HeaderText = "Propietario";
            this.Propietario.MaxInputLength = 15;
            this.Propietario.Name = "Propietario";
            this.Propietario.ReadOnly = true;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre del Animal";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            // 
            // nHijos
            // 
            this.nHijos.HeaderText = "No. Hijos";
            this.nHijos.Name = "nHijos";
            this.nHijos.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            // 
            // lblControlPartos
            // 
            this.lblControlPartos.AutoSize = true;
            this.lblControlPartos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControlPartos.Location = new System.Drawing.Point(13, 489);
            this.lblControlPartos.Name = "lblControlPartos";
            this.lblControlPartos.Size = new System.Drawing.Size(118, 15);
            this.lblControlPartos.TabIndex = 19;
            this.lblControlPartos.Text = "Control de Partos";
            // 
            // lblUltParto
            // 
            this.lblUltParto.AutoSize = true;
            this.lblUltParto.Location = new System.Drawing.Point(63, 525);
            this.lblUltParto.Name = "lblUltParto";
            this.lblUltParto.Size = new System.Drawing.Size(100, 13);
            this.lblUltParto.TabIndex = 20;
            this.lblUltParto.Text = "Fecha Último Parto:";
            // 
            // lblMesesAbiertos
            // 
            this.lblMesesAbiertos.AutoSize = true;
            this.lblMesesAbiertos.Location = new System.Drawing.Point(63, 560);
            this.lblMesesAbiertos.Name = "lblMesesAbiertos";
            this.lblMesesAbiertos.Size = new System.Drawing.Size(82, 13);
            this.lblMesesAbiertos.TabIndex = 21;
            this.lblMesesAbiertos.Text = "Meses Abiertos:";
            // 
            // lblDiasAbiertos
            // 
            this.lblDiasAbiertos.AutoSize = true;
            this.lblDiasAbiertos.Location = new System.Drawing.Point(334, 560);
            this.lblDiasAbiertos.Name = "lblDiasAbiertos";
            this.lblDiasAbiertos.Size = new System.Drawing.Size(74, 13);
            this.lblDiasAbiertos.TabIndex = 22;
            this.lblDiasAbiertos.Text = "Días Abiertos:";
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(285, 604);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(123, 38);
            this.btnVolver.TabIndex = 23;
            this.btnVolver.Text = "VOLVER";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // txtUbicacion
            // 
            this.txtUbicacion.Location = new System.Drawing.Point(433, 58);
            this.txtUbicacion.Name = "txtUbicacion";
            this.txtUbicacion.ReadOnly = true;
            this.txtUbicacion.Size = new System.Drawing.Size(129, 20);
            this.txtUbicacion.TabIndex = 24;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(130, 93);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(122, 20);
            this.txtNombre.TabIndex = 25;
            // 
            // txtArete
            // 
            this.txtArete.Location = new System.Drawing.Point(433, 93);
            this.txtArete.Name = "txtArete";
            this.txtArete.ReadOnly = true;
            this.txtArete.Size = new System.Drawing.Size(129, 20);
            this.txtArete.TabIndex = 26;
            // 
            // txtSexo
            // 
            this.txtSexo.Location = new System.Drawing.Point(130, 129);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.ReadOnly = true;
            this.txtSexo.Size = new System.Drawing.Size(122, 20);
            this.txtSexo.TabIndex = 27;
            // 
            // txtEstado
            // 
            this.txtEstado.Location = new System.Drawing.Point(433, 129);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.ReadOnly = true;
            this.txtEstado.Size = new System.Drawing.Size(129, 20);
            this.txtEstado.TabIndex = 28;
            // 
            // txtRGD
            // 
            this.txtRGD.Location = new System.Drawing.Point(130, 166);
            this.txtRGD.Name = "txtRGD";
            this.txtRGD.ReadOnly = true;
            this.txtRGD.Size = new System.Drawing.Size(122, 20);
            this.txtRGD.TabIndex = 29;
            // 
            // txtRaza
            // 
            this.txtRaza.Location = new System.Drawing.Point(433, 166);
            this.txtRaza.Name = "txtRaza";
            this.txtRaza.ReadOnly = true;
            this.txtRaza.Size = new System.Drawing.Size(129, 20);
            this.txtRaza.TabIndex = 30;
            // 
            // txtPropietario
            // 
            this.txtPropietario.Location = new System.Drawing.Point(130, 200);
            this.txtPropietario.Name = "txtPropietario";
            this.txtPropietario.ReadOnly = true;
            this.txtPropietario.Size = new System.Drawing.Size(122, 20);
            this.txtPropietario.TabIndex = 31;
            // 
            // txtClasificacion
            // 
            this.txtClasificacion.Location = new System.Drawing.Point(433, 200);
            this.txtClasificacion.Name = "txtClasificacion";
            this.txtClasificacion.ReadOnly = true;
            this.txtClasificacion.Size = new System.Drawing.Size(129, 20);
            this.txtClasificacion.TabIndex = 32;
            // 
            // txtFechaNacimiento
            // 
            this.txtFechaNacimiento.Location = new System.Drawing.Point(183, 235);
            this.txtFechaNacimiento.Name = "txtFechaNacimiento";
            this.txtFechaNacimiento.ReadOnly = true;
            this.txtFechaNacimiento.Size = new System.Drawing.Size(144, 20);
            this.txtFechaNacimiento.TabIndex = 33;
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(433, 235);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.ReadOnly = true;
            this.txtEdad.Size = new System.Drawing.Size(129, 20);
            this.txtEdad.TabIndex = 34;
            // 
            // txtFechaLlegada
            // 
            this.txtFechaLlegada.Location = new System.Drawing.Point(183, 269);
            this.txtFechaLlegada.Name = "txtFechaLlegada";
            this.txtFechaLlegada.ReadOnly = true;
            this.txtFechaLlegada.Size = new System.Drawing.Size(144, 20);
            this.txtFechaLlegada.TabIndex = 35;
            // 
            // txtHaciendaOrigen
            // 
            this.txtHaciendaOrigen.Location = new System.Drawing.Point(433, 269);
            this.txtHaciendaOrigen.Name = "txtHaciendaOrigen";
            this.txtHaciendaOrigen.ReadOnly = true;
            this.txtHaciendaOrigen.Size = new System.Drawing.Size(143, 20);
            this.txtHaciendaOrigen.TabIndex = 36;
            // 
            // txtFechaUltParto
            // 
            this.txtFechaUltParto.Location = new System.Drawing.Point(169, 522);
            this.txtFechaUltParto.Name = "txtFechaUltParto";
            this.txtFechaUltParto.ReadOnly = true;
            this.txtFechaUltParto.Size = new System.Drawing.Size(138, 20);
            this.txtFechaUltParto.TabIndex = 37;
            // 
            // txtMesesAbiertos
            // 
            this.txtMesesAbiertos.Location = new System.Drawing.Point(169, 557);
            this.txtMesesAbiertos.Name = "txtMesesAbiertos";
            this.txtMesesAbiertos.ReadOnly = true;
            this.txtMesesAbiertos.Size = new System.Drawing.Size(138, 20);
            this.txtMesesAbiertos.TabIndex = 38;
            // 
            // txtDiasAbiertos
            // 
            this.txtDiasAbiertos.Location = new System.Drawing.Point(433, 557);
            this.txtDiasAbiertos.Name = "txtDiasAbiertos";
            this.txtDiasAbiertos.ReadOnly = true;
            this.txtDiasAbiertos.Size = new System.Drawing.Size(129, 20);
            this.txtDiasAbiertos.TabIndex = 39;
            // 
            // ConsultaInfoAnimal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 663);
            this.Controls.Add(this.txtDiasAbiertos);
            this.Controls.Add(this.txtMesesAbiertos);
            this.Controls.Add(this.txtFechaUltParto);
            this.Controls.Add(this.txtHaciendaOrigen);
            this.Controls.Add(this.txtFechaLlegada);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.txtFechaNacimiento);
            this.Controls.Add(this.txtClasificacion);
            this.Controls.Add(this.txtPropietario);
            this.Controls.Add(this.txtRaza);
            this.Controls.Add(this.txtRGD);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.txtSexo);
            this.Controls.Add(this.txtArete);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtUbicacion);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.lblDiasAbiertos);
            this.Controls.Add(this.lblMesesAbiertos);
            this.Controls.Add(this.lblUltParto);
            this.Controls.Add(this.lblControlPartos);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblGenealogia);
            this.Controls.Add(this.lblHaciendaOrigen);
            this.Controls.Add(this.lblEdad);
            this.Controls.Add(this.lblFechaLlegada);
            this.Controls.Add(this.lblFechaNacimiento);
            this.Controls.Add(this.lblClasificacion);
            this.Controls.Add(this.lblPropietario);
            this.Controls.Add(this.lblRaza);
            this.Controls.Add(this.lblRGD);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.lblArete);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblUbicacion);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.lblRP);
            this.Controls.Add(this.lnkEditarDatos);
            this.Controls.Add(this.lblDatosGenerales);
            this.Name = "ConsultaInfoAnimal";
            this.Text = "Consulta Información del Animal";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDatosGenerales;
        private System.Windows.Forms.LinkLabel lnkEditarDatos;
        private System.Windows.Forms.Label lblRP;
        private System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblArete;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblRGD;
        private System.Windows.Forms.Label lblRaza;
        private System.Windows.Forms.Label lblPropietario;
        private System.Windows.Forms.Label lblClasificacion;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.Label lblFechaLlegada;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblHaciendaOrigen;
        private System.Windows.Forms.Label lblGenealogia;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblControlPartos;
        private System.Windows.Forms.Label lblUltParto;
        private System.Windows.Forms.Label lblMesesAbiertos;
        private System.Windows.Forms.Label lblDiasAbiertos;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.TextBox txtUbicacion;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtArete;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.TextBox txtRGD;
        private System.Windows.Forms.TextBox txtRaza;
        private System.Windows.Forms.TextBox txtPropietario;
        private System.Windows.Forms.TextBox txtClasificacion;
        private System.Windows.Forms.TextBox txtFechaNacimiento;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.TextBox txtFechaLlegada;
        private System.Windows.Forms.TextBox txtHaciendaOrigen;
        private System.Windows.Forms.TextBox txtFechaUltParto;
        private System.Windows.Forms.TextBox txtMesesAbiertos;
        private System.Windows.Forms.TextBox txtDiasAbiertos;
        private System.Windows.Forms.DataGridViewTextBoxColumn RP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Relacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Propietario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn nHijos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
    }
}