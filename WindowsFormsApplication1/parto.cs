﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class parto
    {
        public string rp_padre { get; set; }
        public string fecha_parto { get; set; }
        public string sexo { get; set; }
        public string estado { get; set; }
        public string id_parto { get; set; }
        public string rp_vaca { get; set; }
        
        public parto() { }

        public parto(string rp_padre, string fecha_parto, string sexo, string estado, string id_parto, string rp_vaca)
        {
            this.rp_padre = rp_padre;
            this.fecha_parto = fecha_parto;
            this.sexo = sexo;
            this.estado = estado;
            this.id_parto = id_parto;
            this.rp_vaca = rp_vaca;
        }
    }
}
