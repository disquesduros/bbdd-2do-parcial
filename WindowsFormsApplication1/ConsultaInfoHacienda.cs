﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class ConsultaInfoHacienda : Form
    {
        public ConsultaInfoHacienda()
        {
            InitializeComponent();
        }

        private void lnkEditar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoDatosHacienda datos = new IngresoDatosHacienda();
            datos.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            datos.Show();
            this.Hide();
        }

        private void lnkNuevoArea_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoInfoArea area = new IngresoInfoArea(false);
            area.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            area.Show();
            this.Hide();
        }

        private void lnkNuevoPersonal_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoPersonas personal = new WindowsFormsApplication1.IngresoPersonas(false);
            personal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            personal.Show();
            this.Hide();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void dvgArea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                IngresoInfoArea area = new IngresoInfoArea(true);
                
                string celda= dvgArea.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_areaNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom",celda));
                
                area.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                area.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
<<<<<<< HEAD
<<<<<<< HEAD

<<<<<<< HEAD
<<<<<<< HEAD
                area.txtcod.Text = reader.GetString(0);

=======
               
>>>>>>> 38e5dc68151f8f0c8abe6d521b5d58c424506564
=======
               
=======
                area.txtcod.Text = reader.GetString(0);
>>>>>>> parent of 620ed6f... busqueda animales listo
=======
>>>>>>> parent of e631818... adios
>>>>>>> parent of fe16d3c... prueba
                area.txtNombre.Text = reader.GetString(1);
                area.txtDireccion.Text= reader.GetString(2);
                area.txtUbicacion.Text = reader.GetString(3);
                area.txtDimensiones.Text = reader.GetString(4);
                area.txtCapacidadMax.Text = reader.GetString(5);
                area.txtCodigo.Text = reader.GetString(0);

                
                this.Hide();
            }
        }

        private void dgvPersona_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                WindowsFormsApplication1.IngresoPersonas persona = new WindowsFormsApplication1.IngresoPersonas(true);
                string celda = dgvPersona.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_personNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom",celda));

                persona.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                persona.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                persona.txtcedula.Text= reader.GetString(0);
                persona.txtnombre.Text = reader.GetString(1);
                persona.textrol.Text = reader.GetString(2);
                persona.txtdireccion.Text = reader.GetString(3);
                persona.txtconvencional.Text = reader.GetString(4);
                persona.txtmovil.Text = reader.GetString(5);


                this.Hide();
            }
        }

        private void ConsultaInfoHacienda_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_area", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dvgArea.Rows.Add(true,reader.GetString(1), reader.GetString(4), reader.GetString(5), reader.GetString(2), reader.GetString(3));

            MySqlCommand cmd1 = new MySqlCommand("buscar_persona", coneccionBase.ObtenerConexion());
            cmd1.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader1 = cmd1.ExecuteReader();
            while (reader1.Read())
                dgvPersona.Rows.Add(true, reader1.GetString(0), reader1.GetString(1), reader1.GetString(3), reader1.GetString(5), reader1.GetString(4));
        }

        private void lnkEliminarArea_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dvgArea.SelectedRows.Count == 1)
            {
                string nom = Convert.ToString(dvgArea.CurrentRow.Cells[1].Value);
                areaAcciones.Eliminar(nom);
                dvgArea.Rows.Remove(dvgArea.CurrentRow);
               
            }
            else
                MessageBox.Show("Debe seleccionar una fila");

        }

       

        private void lnkEliminarPersonal_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPersona.SelectedRows.Count == 1)
            {
                string ced = Convert.ToString(dvgArea.CurrentRow.Cells[1].Value);
                personaAcciones.Eliminar(ced);
                dgvPersona.Rows.Remove(dgvPersona.CurrentRow);

            }
            else
                MessageBox.Show("Debe seleccionar una fila");
        }

        private void dvgArea_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                IngresoInfoArea area = new IngresoInfoArea(true);

                string celda = dvgArea.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_areaNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom", celda));

                area.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                area.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                area.txtNombre.Text = reader.GetString(1);
                area.txtDireccion.Text = reader.GetString(2);
                area.txtUbicacion.Text = reader.GetString(3);
                area.txtDimensiones.Text = reader.GetString(4);
                area.txtCapacidadMax.Text = reader.GetString(5);
                area.txtCodigo.Text = reader.GetString(0);
                this.Hide();
            }
        }

        private void dgvPersona_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                WindowsFormsApplication1.IngresoPersonas persona = new WindowsFormsApplication1.IngresoPersonas(true);
                string celda = dgvPersona.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_personNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom", celda));

                persona.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                persona.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                persona.txtcedula.Text = reader.GetString(0);
                persona.txtnombre.Text = reader.GetString(1);
                persona.txtdireccion.Text = reader.GetString(3);
                persona.txtconvencional.Text = reader.GetString(4);
                persona.txtmovil.Text = reader.GetString(5);
                persona.txtId.Text = reader.GetString(6);

                this.Hide();
            }
        }
    }
}
