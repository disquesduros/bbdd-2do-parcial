﻿namespace WindowsFormsApplication1
{
    partial class GeneracionReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneracionReportes));
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblFechaHasta = new System.Windows.Forms.Label();
            this.lblFechaDesde = new System.Windows.Forms.Label();
            this.btnvolver = new System.Windows.Forms.Button();
            this.btnReporte = new System.Windows.Forms.Button();
            this.chkBjas = new System.Windows.Forms.CheckBox();
            this.chkReproduccion = new System.Windows.Forms.CheckBox();
            this.chkSanitario = new System.Windows.Forms.CheckBox();
            this.chkIngresosGastos = new System.Windows.Forms.CheckBox();
            this.chkPesajes = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(486, 37);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 26;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(89, 37);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 25;
            // 
            // lblFechaHasta
            // 
            this.lblFechaHasta.AutoSize = true;
            this.lblFechaHasta.BackColor = System.Drawing.SystemColors.Control;
            this.lblFechaHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaHasta.Location = new System.Drawing.Point(408, 37);
            this.lblFechaHasta.Name = "lblFechaHasta";
            this.lblFechaHasta.Size = new System.Drawing.Size(69, 13);
            this.lblFechaHasta.TabIndex = 24;
            this.lblFechaHasta.Text = "Fecha hasta:";
            // 
            // lblFechaDesde
            // 
            this.lblFechaDesde.AutoSize = true;
            this.lblFechaDesde.BackColor = System.Drawing.SystemColors.Control;
            this.lblFechaDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaDesde.Location = new System.Drawing.Point(7, 37);
            this.lblFechaDesde.Name = "lblFechaDesde";
            this.lblFechaDesde.Size = new System.Drawing.Size(72, 13);
            this.lblFechaDesde.TabIndex = 23;
            this.lblFechaDesde.Text = "Fecha desde:";
            // 
            // btnvolver
            // 
            this.btnvolver.Location = new System.Drawing.Point(424, 267);
            this.btnvolver.Name = "btnvolver";
            this.btnvolver.Size = new System.Drawing.Size(95, 40);
            this.btnvolver.TabIndex = 27;
            this.btnvolver.Text = "Volver";
            this.btnvolver.UseVisualStyleBackColor = true;
            this.btnvolver.Click += new System.EventHandler(this.btnvolver_Click);
            // 
            // btnReporte
            // 
            this.btnReporte.Location = new System.Drawing.Point(243, 267);
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(105, 40);
            this.btnReporte.TabIndex = 28;
            this.btnReporte.Text = "Generar Reporte";
            this.btnReporte.UseVisualStyleBackColor = true;
            // 
            // chkBjas
            // 
            this.chkBjas.AutoSize = true;
            this.chkBjas.Location = new System.Drawing.Point(291, 82);
            this.chkBjas.Name = "chkBjas";
            this.chkBjas.Size = new System.Drawing.Size(108, 17);
            this.chkBjas.TabIndex = 29;
            this.chkBjas.Text = "Reporte de Bajas";
            this.chkBjas.UseVisualStyleBackColor = true;
            // 
            // chkReproduccion
            // 
            this.chkReproduccion.AutoSize = true;
            this.chkReproduccion.Location = new System.Drawing.Point(291, 116);
            this.chkReproduccion.Name = "chkReproduccion";
            this.chkReproduccion.Size = new System.Drawing.Size(186, 17);
            this.chkReproduccion.TabIndex = 30;
            this.chkReproduccion.Text = "Reporte de Planes Reproductivos";
            this.chkReproduccion.UseVisualStyleBackColor = true;
            this.chkReproduccion.CheckedChanged += new System.EventHandler(this.chkReproduccion_CheckedChanged);
            // 
            // chkSanitario
            // 
            this.chkSanitario.AutoSize = true;
            this.chkSanitario.Location = new System.Drawing.Point(291, 150);
            this.chkSanitario.Name = "chkSanitario";
            this.chkSanitario.Size = new System.Drawing.Size(163, 17);
            this.chkSanitario.TabIndex = 31;
            this.chkSanitario.Text = "Reporte de Planes Sanitarios";
            this.chkSanitario.UseVisualStyleBackColor = true;
            // 
            // chkIngresosGastos
            // 
            this.chkIngresosGastos.AutoSize = true;
            this.chkIngresosGastos.Location = new System.Drawing.Point(291, 187);
            this.chkIngresosGastos.Name = "chkIngresosGastos";
            this.chkIngresosGastos.Size = new System.Drawing.Size(166, 17);
            this.chkIngresosGastos.TabIndex = 32;
            this.chkIngresosGastos.Text = "Reporte de Ingresos y Gastos";
            this.chkIngresosGastos.UseVisualStyleBackColor = true;
            // 
            // chkPesajes
            // 
            this.chkPesajes.AutoSize = true;
            this.chkPesajes.Location = new System.Drawing.Point(291, 227);
            this.chkPesajes.Name = "chkPesajes";
            this.chkPesajes.Size = new System.Drawing.Size(119, 17);
            this.chkPesajes.TabIndex = 33;
            this.chkPesajes.Text = "Reporte de Pesajes";
            this.chkPesajes.UseVisualStyleBackColor = true;
            // 
            // GeneracionReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 337);
            this.Controls.Add(this.chkPesajes);
            this.Controls.Add(this.chkIngresosGastos);
            this.Controls.Add(this.chkSanitario);
            this.Controls.Add(this.chkReproduccion);
            this.Controls.Add(this.chkBjas);
            this.Controls.Add(this.btnReporte);
            this.Controls.Add(this.btnvolver);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.lblFechaHasta);
            this.Controls.Add(this.lblFechaDesde);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GeneracionReportes";
            this.Text = "Generacion de Reportes";
            this.Load += new System.EventHandler(this.GeneracionReportes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblFechaHasta;
        private System.Windows.Forms.Label lblFechaDesde;
        private System.Windows.Forms.Button btnvolver;
        private System.Windows.Forms.Button btnReporte;
        private System.Windows.Forms.CheckBox chkBjas;
        private System.Windows.Forms.CheckBox chkReproduccion;
        private System.Windows.Forms.CheckBox chkSanitario;
        private System.Windows.Forms.CheckBox chkIngresosGastos;
        private System.Windows.Forms.CheckBox chkPesajes;
    }
}