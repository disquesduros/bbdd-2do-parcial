﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoPesajes : Form
    {
        public IngresoPesajes()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtRP.Text)||string.IsNullOrWhiteSpace(txtPeso.Text)||string.IsNullOrWhiteSpace(datePesaje.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtRP.Text.Trim(' ').Length != 3)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RP (deben ser 3 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else

            {
                pesaje pesaje = new pesaje();
                pesaje.id_pesaje = txtIDPesaje.Text.Trim();
                pesaje.rp_vaca = txtRP.Text.Trim();
                pesaje.peso = (float)Convert.ToDouble(txtPeso.Text);
                pesaje.fecha_pesaje= datePesaje.Value.Year + "/" + datePesaje.Value.Month + "/" + datePesaje.Value.Day;
                int resultado = pesajeAcciones.Agregar(pesaje);
                if (resultado > 0)
                {
                    MessageBox.Show("Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtRP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }

        }

        private static void SoloNumeros(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e,false);

        }

        private void IngresoPesajes_Load(object sender, EventArgs e)
        {

        }

        private void txtRP_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
