﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class animal
    {
        public string rp { get; set; }
        public string n_arete { get; set; }
        public string id_area { get; set; }
        public string nombre { get; set; }
        public string sexo { get; set; }
        public string fecha_llega { get; set; }
        public string haci_ori { get; set; }
        public string propi { get; set; }
        public string rp_madre { get; set; }
        public string rp_padre { get; set; }
        public string estado { get; set; }
        public string raza { get; set; }
        public animal() { }

        public animal(string rp, string n_arete, string id_area, string nombre, string sexo, string fecha_llega,string haci_ori, string propi, string rp_madre, string rp_padre, string estado, string raza)
        {
            this.rp = rp;
            this.n_arete = n_arete;
            this.id_area = id_area;
            this.nombre = nombre;
            this.sexo = sexo;
            this.fecha_llega = fecha_llega;
            this.haci_ori = haci_ori;
            this.propi = propi;
            this.rp_madre = rp_madre;
            this.rp_padre = rp_padre;
            this.estado = estado;
            this.raza = raza;

        }
    }
}
