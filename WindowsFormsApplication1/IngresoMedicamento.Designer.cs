﻿namespace WindowsFormsApplication1
{
    partial class IngresoMedicamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblTipo = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.lblPeriodicidad = new System.Windows.Forms.Label();
            this.lblPosologia = new System.Windows.Forms.Label();
            this.txtPeriodicidad = new System.Windows.Forms.TextBox();
            this.txtPosologia = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(61, 47);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(138, 39);
            this.txtNombre.MaxLength = 20;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(127, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(293, 47);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(98, 13);
            this.lblTipo.TabIndex = 2;
            this.lblTipo.Text = "Tipo de Aplicación:";
            // 
            // cmbTipo
            // 
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "INTERNO",
            "EXTERNO"});
            this.cmbTipo.Location = new System.Drawing.Point(397, 39);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(125, 21);
            this.cmbTipo.TabIndex = 3;
            // 
            // lblPeriodicidad
            // 
            this.lblPeriodicidad.AutoSize = true;
            this.lblPeriodicidad.Location = new System.Drawing.Point(61, 85);
            this.lblPeriodicidad.Name = "lblPeriodicidad";
            this.lblPeriodicidad.Size = new System.Drawing.Size(71, 13);
            this.lblPeriodicidad.TabIndex = 4;
            this.lblPeriodicidad.Text = "Periodicidad: ";
            // 
            // lblPosologia
            // 
            this.lblPosologia.AutoSize = true;
            this.lblPosologia.Location = new System.Drawing.Point(61, 120);
            this.lblPosologia.Name = "lblPosologia";
            this.lblPosologia.Size = new System.Drawing.Size(58, 13);
            this.lblPosologia.TabIndex = 5;
            this.lblPosologia.Text = "Posología:";
            // 
            // txtPeriodicidad
            // 
            this.txtPeriodicidad.Location = new System.Drawing.Point(138, 82);
            this.txtPeriodicidad.MaxLength = 10;
            this.txtPeriodicidad.Name = "txtPeriodicidad";
            this.txtPeriodicidad.Size = new System.Drawing.Size(127, 20);
            this.txtPeriodicidad.TabIndex = 6;
            // 
            // txtPosologia
            // 
            this.txtPosologia.Location = new System.Drawing.Point(138, 117);
            this.txtPosologia.MaxLength = 20;
            this.txtPosologia.Name = "txtPosologia";
            this.txtPosologia.Size = new System.Drawing.Size(127, 20);
            this.txtPosologia.TabIndex = 7;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(138, 198);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(127, 44);
            this.btnGuardar.TabIndex = 8;
            this.btnGuardar.Text = "GUARDAR/CERRAR";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnDescartar
            // 
            this.btnDescartar.Location = new System.Drawing.Point(334, 198);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(127, 44);
            this.btnDescartar.TabIndex = 9;
            this.btnDescartar.Text = "DESCARTAR";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            // 
            // IngresoMedicamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 265);
            this.Controls.Add(this.btnDescartar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtPosologia);
            this.Controls.Add(this.txtPeriodicidad);
            this.Controls.Add(this.lblPosologia);
            this.Controls.Add(this.lblPeriodicidad);
            this.Controls.Add(this.cmbTipo);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNombre);
            this.Name = "IngresoMedicamento";
            this.Text = "Ingreso Información de Medicamento";
            this.Load += new System.EventHandler(this.IngresoMedicamento_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Label lblPeriodicidad;
        private System.Windows.Forms.Label lblPosologia;
        private System.Windows.Forms.TextBox txtPeriodicidad;
        private System.Windows.Forms.TextBox txtPosologia;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnDescartar;
    }
}