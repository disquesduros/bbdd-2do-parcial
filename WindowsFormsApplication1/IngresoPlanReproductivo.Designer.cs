﻿namespace WindowsFormsApplication1
{
    partial class IngresoPlanReproductivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblVaca = new System.Windows.Forms.Label();
            this.txtVaca = new System.Windows.Forms.TextBox();
            this.lblToro = new System.Windows.Forms.Label();
            this.txtToro = new System.Windows.Forms.TextBox();
            this.lblCondicion = new System.Windows.Forms.Label();
            this.txtCondicion = new System.Windows.Forms.TextBox();
            this.lblTipo = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.lblComentario = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.datePlan = new System.Windows.Forms.DateTimePicker();
            this.chbDispo = new System.Windows.Forms.CheckBox();
            this.chbPrena = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtIDRepro = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblVaca
            // 
            this.lblVaca.AutoSize = true;
            this.lblVaca.Location = new System.Drawing.Point(50, 33);
            this.lblVaca.Name = "lblVaca";
            this.lblVaca.Size = new System.Drawing.Size(53, 13);
            this.lblVaca.TabIndex = 0;
            this.lblVaca.Text = "RP Vaca:";
            // 
            // txtVaca
            // 
            this.txtVaca.Location = new System.Drawing.Point(159, 30);
            this.txtVaca.MaxLength = 3;
            this.txtVaca.Name = "txtVaca";
            this.txtVaca.Size = new System.Drawing.Size(100, 20);
            this.txtVaca.TabIndex = 1;
            this.txtVaca.TextChanged += new System.EventHandler(this.txtVaca_TextChanged);
            this.txtVaca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVaca_KeyPress);
            // 
            // lblToro
            // 
            this.lblToro.AutoSize = true;
            this.lblToro.Location = new System.Drawing.Point(282, 33);
            this.lblToro.Name = "lblToro";
            this.lblToro.Size = new System.Drawing.Size(50, 13);
            this.lblToro.TabIndex = 2;
            this.lblToro.Text = "RP Toro:";
            // 
            // txtToro
            // 
            this.txtToro.Location = new System.Drawing.Point(352, 30);
            this.txtToro.MaxLength = 3;
            this.txtToro.Name = "txtToro";
            this.txtToro.Size = new System.Drawing.Size(100, 20);
            this.txtToro.TabIndex = 3;
            this.txtToro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtToro_KeyPress);
            // 
            // lblCondicion
            // 
            this.lblCondicion.AutoSize = true;
            this.lblCondicion.Location = new System.Drawing.Point(50, 68);
            this.lblCondicion.Name = "lblCondicion";
            this.lblCondicion.Size = new System.Drawing.Size(99, 13);
            this.lblCondicion.TabIndex = 4;
            this.lblCondicion.Text = "Condición Corporal:";
            // 
            // txtCondicion
            // 
            this.txtCondicion.Location = new System.Drawing.Point(159, 65);
            this.txtCondicion.MaxLength = 3;
            this.txtCondicion.Name = "txtCondicion";
            this.txtCondicion.Size = new System.Drawing.Size(100, 20);
            this.txtCondicion.TabIndex = 5;
            this.txtCondicion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCondicion_KeyPress);
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(50, 101);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(31, 13);
            this.lblTipo.TabIndex = 6;
            this.lblTipo.Text = "Tipo:";
            // 
            // cmbTipo
            // 
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "MONTA NATURAL",
            "INSEMINACIÓN ARTIFICIAL"});
            this.cmbTipo.Location = new System.Drawing.Point(159, 98);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(140, 21);
            this.cmbTipo.TabIndex = 7;
            // 
            // lblComentario
            // 
            this.lblComentario.AutoSize = true;
            this.lblComentario.Location = new System.Drawing.Point(50, 132);
            this.lblComentario.Name = "lblComentario";
            this.lblComentario.Size = new System.Drawing.Size(63, 13);
            this.lblComentario.TabIndex = 8;
            this.lblComentario.Text = "Comentario:";
            // 
            // txtComentario
            // 
            this.txtComentario.Location = new System.Drawing.Point(159, 129);
            this.txtComentario.MaxLength = 100;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(257, 47);
            this.txtComentario.TabIndex = 9;
            this.txtComentario.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 193);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Fecha de Plan:";
            // 
            // datePlan
            // 
            this.datePlan.Location = new System.Drawing.Point(159, 187);
            this.datePlan.Name = "datePlan";
            this.datePlan.Size = new System.Drawing.Size(200, 20);
            this.datePlan.TabIndex = 11;
            // 
            // chbDispo
            // 
            this.chbDispo.AutoSize = true;
            this.chbDispo.Location = new System.Drawing.Point(53, 228);
            this.chbDispo.Name = "chbDispo";
            this.chbDispo.Size = new System.Drawing.Size(122, 17);
            this.chbDispo.TabIndex = 12;
            this.chbDispo.Text = "¿Aplica dispositivo? ";
            this.chbDispo.UseVisualStyleBackColor = true;
            // 
            // chbPrena
            // 
            this.chbPrena.AutoSize = true;
            this.chbPrena.Location = new System.Drawing.Point(53, 252);
            this.chbPrena.Name = "chbPrena";
            this.chbPrena.Size = new System.Drawing.Size(104, 17);
            this.chbPrena.TabIndex = 13;
            this.chbPrena.Text = "¿Está preñada? ";
            this.chbPrena.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(122, 288);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 36);
            this.button1.TabIndex = 14;
            this.button1.Text = "Guardar/Cerrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(335, 288);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 36);
            this.button2.TabIndex = 15;
            this.button2.Text = "Descartar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtIDRepro
            // 
            this.txtIDRepro.Location = new System.Drawing.Point(436, 98);
            this.txtIDRepro.MaxLength = 3;
            this.txtIDRepro.Name = "txtIDRepro";
            this.txtIDRepro.Size = new System.Drawing.Size(100, 20);
            this.txtIDRepro.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(321, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(249, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "id_planRepro(no se si esto este campo se agregue)";
            // 
            // IngresoPlanReproductivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 336);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIDRepro);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chbPrena);
            this.Controls.Add(this.chbDispo);
            this.Controls.Add(this.datePlan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtComentario);
            this.Controls.Add(this.lblComentario);
            this.Controls.Add(this.cmbTipo);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.txtCondicion);
            this.Controls.Add(this.lblCondicion);
            this.Controls.Add(this.txtToro);
            this.Controls.Add(this.lblToro);
            this.Controls.Add(this.txtVaca);
            this.Controls.Add(this.lblVaca);
            this.Name = "IngresoPlanReproductivo";
            this.Text = "Ingreso de Plan Reproductivo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVaca;
        private System.Windows.Forms.TextBox txtVaca;
        private System.Windows.Forms.Label lblToro;
        private System.Windows.Forms.TextBox txtToro;
        private System.Windows.Forms.Label lblCondicion;
        private System.Windows.Forms.TextBox txtCondicion;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Label lblComentario;
        private System.Windows.Forms.RichTextBox txtComentario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datePlan;
        private System.Windows.Forms.CheckBox chbDispo;
        private System.Windows.Forms.CheckBox chbPrena;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtIDRepro;
        private System.Windows.Forms.Label label2;
    }
}