﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class IngresoBajas : Form
    {
        
        public IngresoBajas()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private static void SoloNumeros(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }
        private void txtrp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtpeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e, false);
        }

        private void txtprecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e, false);
        }
        private void txtdetalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void IngresoBajas_Load(object sender, EventArgs e)
        {

        }

        private void txtdetalle_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtrp_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpeso_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtrp.Text)||string.IsNullOrWhiteSpace(dtfecha.Text)||string.IsNullOrWhiteSpace(cmbtipo.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {

                baja baja = new baja();
                baja.peso = float.Parse(txtpeso.Text);
                baja.fecha= dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
                baja.preciolb = float.Parse(txtprecio.Text);
                baja.rpvaca = txtrp.Text;
                int resultado = bajaAcciones.Agregar(baja);
                if (resultado > 0)
                {
                    MessageBox.Show("Baja Guardada Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar la baja", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btndescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
