﻿namespace WindowsFormsApplication1
{
    partial class IngresoPesajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRP = new System.Windows.Forms.Label();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.lblPeso = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.datePesaje = new System.Windows.Forms.DateTimePicker();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.txtIDPesaje = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.Location = new System.Drawing.Point(47, 39);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(25, 13);
            this.lblRP.TabIndex = 0;
            this.lblRP.Text = "RP:";
            // 
            // txtRP
            // 
            this.txtRP.Location = new System.Drawing.Point(89, 36);
            this.txtRP.MaxLength = 3;
            this.txtRP.Name = "txtRP";
            this.txtRP.Size = new System.Drawing.Size(100, 20);
            this.txtRP.TabIndex = 1;
            this.txtRP.TextChanged += new System.EventHandler(this.txtRP_TextChanged);
            this.txtRP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRP_KeyPress);
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Location = new System.Drawing.Point(47, 75);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(34, 13);
            this.lblPeso.TabIndex = 2;
            this.lblPeso.Text = "Peso:";
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(89, 72);
            this.txtPeso.MaxLength = 10;
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(100, 20);
            this.txtPeso.TabIndex = 3;
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Fecha del Pesaje:";
            // 
            // datePesaje
            // 
            this.datePesaje.Location = new System.Drawing.Point(145, 104);
            this.datePesaje.Name = "datePesaje";
            this.datePesaje.Size = new System.Drawing.Size(200, 20);
            this.datePesaje.TabIndex = 5;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(89, 169);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(99, 37);
            this.btnGuardar.TabIndex = 6;
            this.btnGuardar.Text = "Guardar/ Cerrar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnDescartar
            // 
            this.btnDescartar.Location = new System.Drawing.Point(259, 169);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(99, 37);
            this.btnDescartar.TabIndex = 7;
            this.btnDescartar.Text = "Volver";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            // 
            // txtIDPesaje
            // 
            this.txtIDPesaje.Location = new System.Drawing.Point(298, 36);
            this.txtIDPesaje.MaxLength = 3;
            this.txtIDPesaje.Name = "txtIDPesaje";
            this.txtIDPesaje.Size = new System.Drawing.Size(100, 20);
            this.txtIDPesaje.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(256, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "idpesaje(no se si se agregue este campo)";
            // 
            // IngresoPesajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 235);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIDPesaje);
            this.Controls.Add(this.btnDescartar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.datePesaje);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.lblRP);
            this.Name = "IngresoPesajes";
            this.Text = "Ingreso de Información de Pesaje";
            this.Load += new System.EventHandler(this.IngresoPesajes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRP;
        private System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datePesaje;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnDescartar;
        private System.Windows.Forms.TextBox txtIDPesaje;
        private System.Windows.Forms.Label label2;
    }
}