﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class InformacionPlanesReproductivoPorVaca : Form
    {
        public InformacionPlanesReproductivoPorVaca()
        {
            InitializeComponent();
        }

        private void historialDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                WindowsFormsApplication1.IngresoPlanReproductivo plan = new WindowsFormsApplication1.IngresoPlanReproductivo();
                plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                plan.Show();
                this.Hide();
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lnkNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoPlanReproductivo plan = new WindowsFormsApplication1.IngresoPlanReproductivo();
            plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            plan.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void InformacionPlanesReproductivoPorVaca_Load(object sender, EventArgs e)
        {

        }

        private void lnkEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvplanrepro.SelectedRows.Count == 1)
            {
                string idPlan = Convert.ToString(dgvplanrepro.CurrentRow.Cells[1].Value);
                planReproAcciones.Eliminar(idPlan);
                dgvplanrepro.Rows.Remove(dgvplanrepro.CurrentRow);

            }
            else
                MessageBox.Show("Debe seleccionar una fila");
        }
    }
}
