﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class IngresoInfoAnimalParido : Form
    {
        public IngresoInfoAnimalParido()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtRP.Text)||string.IsNullOrWhiteSpace(cmbEstado.Text)||string.IsNullOrWhiteSpace(cmbRaza.Text)||string.IsNullOrWhiteSpace(cmbPropietario.Text)||string.IsNullOrWhiteSpace(txtPadre.Text)||string.IsNullOrWhiteSpace(txtMadre.Text)/*||string.IsNullOrWhiteSpace(txtRGDPadre.Text)||string.IsNullOrWhiteSpace(txtRGD.Text)*/)

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtRP.Text.Trim(' ').Length != 3 || txtMadre.Text.Trim(' ').Length != 3 || txtPadre.Text.Trim(' ').Length != 3)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RP (deben ser 3 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


            else

            {
                animal animal = new animal();
                animal.rp = txtRP.Text.Trim();
                animal.n_arete = txtArete.Text.Trim();
                animal.id_area = cmbUbicacion.Text.Trim();
                animal.nombre = txtNombre.Text.Trim();
                animal.sexo = cmbSexo.Text.Trim();
                //animal.fecha_llega = dtpFechaLlegada.Value.Year + "/" + dtpFechaLlegada.Value.Month + "/" + dtpFechaLlegada.Value.Day;
                //animal.haci_ori = txtHaciendaOrigen.Text.Trim();
                animal.propi = cmbPropietario.Text.Trim();
                animal.rp_madre = txtMadre.Text.Trim();
                animal.rp_padre = txtPadre.Text.Trim();
                animal.estado = cmbEstado.Text.Trim();
                animal.raza = cmbRaza.Text.Trim();

                int resultado = animalAcciones.Agregar(animal);
                if (resultado > 0)
                {
                    MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }


        private void txtRP_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtArete_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtRDG_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtPadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtMadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtRGDPadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void IngresoInfoAnimalParido_Load(object sender, EventArgs e)
        {
            List<String> _lista = new List<String>();
            MySqlCommand cmd = new MySqlCommand("buscar_persona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {

                _lista.Add(reader.GetString(1));

            }
            cmbPropietario.DataSource = _lista;
        }

        private void txtRP_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
