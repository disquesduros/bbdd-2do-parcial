﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class planSani
    {
        public int id_plan { get; set; }
        public string nombre { get; set; }
        public string tipo { get; set; }
        public string fecha_apli { get; set; }
        public string rp_vaca { get; set; }
        public string nombre_med { get; set; }
        public float dosis { get; set; }
       
        public planSani() { }

        public planSani(int id_plan, string nombre, string tipo,string fecha_apli,string rp_vaca, string nombre_med, float dosis)
        {
            this.id_plan = id_plan;
            this.nombre = nombre;
            this.tipo = tipo;
            this.fecha_apli = fecha_apli;
            this.rp_vaca = rp_vaca;
            this.nombre_med = nombre_med;
            this.dosis = dosis;
           
        }
    }
}
