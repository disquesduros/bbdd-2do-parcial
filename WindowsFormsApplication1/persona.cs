﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class persona
    {
        
        public string ced { get; set; }
        public string nombre { get; set; }
        public string dir { get; set; }
        public string rol { get; set; }
        public string conven { get; set; }
        public string movil { get; set; }
        public int cod { get; set; }
        public persona() { }

        public persona(string ced, string nombre, string dir, string rol,string conven, string movil, int id)
        {
            this.ced = ced;
            this.nombre = nombre;
            this.dir = dir;
            this.rol = rol;
            this.conven = conven;
            this.movil = movil;
            this.cod = id;
        }
    }
}
