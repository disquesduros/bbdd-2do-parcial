﻿namespace WindowsFormsApplication1
{
    partial class ConsultaInfoPesajeporAnimal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRP = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblAnioDesde = new System.Windows.Forms.Label();
            this.lblAniohasta = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IDPesaje = new System.Windows.Forms.DataGridViewLinkColumn();
            this.RPAnimal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaPesaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadAnios = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadMeses = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadDias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Peso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.lblEliminar = new System.Windows.Forms.LinkLabel();
            this.lblNuevo = new System.Windows.Forms.LinkLabel();
            this.btnVolver = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.Location = new System.Drawing.Point(210, 43);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(25, 13);
            this.lblRP.TabIndex = 0;
            this.lblRP.Text = "RP:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(497, 43);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblAnioDesde
            // 
            this.lblAnioDesde.AutoSize = true;
            this.lblAnioDesde.Location = new System.Drawing.Point(210, 85);
            this.lblAnioDesde.Name = "lblAnioDesde";
            this.lblAnioDesde.Size = new System.Drawing.Size(61, 13);
            this.lblAnioDesde.TabIndex = 2;
            this.lblAnioDesde.Text = "Año desde:";
            // 
            // lblAniohasta
            // 
            this.lblAniohasta.AutoSize = true;
            this.lblAniohasta.Location = new System.Drawing.Point(497, 85);
            this.lblAniohasta.Name = "lblAniohasta";
            this.lblAniohasta.Size = new System.Drawing.Size(58, 13);
            this.lblAniohasta.TabIndex = 3;
            this.lblAniohasta.Text = "Año hasta:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.IDPesaje,
            this.RPAnimal,
            this.FechaPesaje,
            this.EdadAnios,
            this.EdadMeses,
            this.EdadDias,
            this.Peso});
            this.dataGridView1.Location = new System.Drawing.Point(12, 135);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(843, 150);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IDPesaje
            // 
            this.IDPesaje.HeaderText = "Código";
            this.IDPesaje.Name = "IDPesaje";
            this.IDPesaje.ReadOnly = true;
            // 
            // RPAnimal
            // 
            this.RPAnimal.HeaderText = "RP Animal";
            this.RPAnimal.Name = "RPAnimal";
            this.RPAnimal.ReadOnly = true;
            // 
            // FechaPesaje
            // 
            this.FechaPesaje.HeaderText = "Fecha Pesaje";
            this.FechaPesaje.Name = "FechaPesaje";
            this.FechaPesaje.ReadOnly = true;
            // 
            // EdadAnios
            // 
            this.EdadAnios.HeaderText = "Edad Años";
            this.EdadAnios.Name = "EdadAnios";
            this.EdadAnios.ReadOnly = true;
            // 
            // EdadMeses
            // 
            this.EdadMeses.HeaderText = "Edad Meses";
            this.EdadMeses.Name = "EdadMeses";
            this.EdadMeses.ReadOnly = true;
            // 
            // EdadDias
            // 
            this.EdadDias.HeaderText = "Edad Días";
            this.EdadDias.Name = "EdadDias";
            this.EdadDias.ReadOnly = true;
            // 
            // Peso
            // 
            this.Peso.HeaderText = "Peso";
            this.Peso.Name = "Peso";
            this.Peso.ReadOnly = true;
            // 
            // txtRP
            // 
            this.txtRP.Location = new System.Drawing.Point(277, 40);
            this.txtRP.Name = "txtRP";
            this.txtRP.ReadOnly = true;
            this.txtRP.Size = new System.Drawing.Size(100, 20);
            this.txtRP.TabIndex = 5;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(561, 40);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 6;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(277, 79);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(561, 79);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // lblEliminar
            // 
            this.lblEliminar.AutoSize = true;
            this.lblEliminar.Location = new System.Drawing.Point(12, 288);
            this.lblEliminar.Name = "lblEliminar";
            this.lblEliminar.Size = new System.Drawing.Size(55, 13);
            this.lblEliminar.TabIndex = 9;
            this.lblEliminar.TabStop = true;
            this.lblEliminar.Text = "Eliminar (-)";
            this.lblEliminar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEliminar_LinkClicked);
            // 
            // lblNuevo
            // 
            this.lblNuevo.AutoSize = true;
            this.lblNuevo.Location = new System.Drawing.Point(798, 288);
            this.lblNuevo.Name = "lblNuevo";
            this.lblNuevo.Size = new System.Drawing.Size(54, 13);
            this.lblNuevo.TabIndex = 10;
            this.lblNuevo.TabStop = true;
            this.lblNuevo.Text = "Nuevo (+)";
            this.lblNuevo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNuevo_LinkClicked);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(365, 306);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(153, 34);
            this.btnVolver.TabIndex = 11;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // ConsultaInfoPesajeporAnimal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 370);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.lblNuevo);
            this.Controls.Add(this.lblEliminar);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblAniohasta);
            this.Controls.Add(this.lblAnioDesde);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblRP);
            this.Name = "ConsultaInfoPesajeporAnimal";
            this.Text = "Consulta de Información Pesaje por Animal";
            this.Load += new System.EventHandler(this.ConsultaInfoPesajeporAnimal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRP;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblAnioDesde;
        private System.Windows.Forms.Label lblAniohasta;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.LinkLabel lblEliminar;
        private System.Windows.Forms.LinkLabel lblNuevo;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewLinkColumn IDPesaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn RPAnimal;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaPesaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadAnios;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadMeses;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadDias;
        private System.Windows.Forms.DataGridViewTextBoxColumn Peso;
    }
}