﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class partoAcciones
    {
        public static int Agregar(parto parto)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_parto", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp_padre", parto.rp_padre));
            cmd.Parameters.Add(new MySqlParameter("fecha_parto", parto.fecha_parto));
            cmd.Parameters.Add(new MySqlParameter("sexo", parto.sexo));
            cmd.Parameters.Add(new MySqlParameter("estado", parto.estado));
            cmd.Parameters.Add(new MySqlParameter("id_parto", parto.id_parto));
            cmd.Parameters.Add(new MySqlParameter("rp_vaca", parto.rp_vaca));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string id_Parto)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_animal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("id_Parto", id_Parto));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }
    }
}
