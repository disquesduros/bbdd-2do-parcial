﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoPlanReproductivo : Form
    {
        public IngresoPlanReproductivo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtVaca.Text)||string.IsNullOrWhiteSpace(txtToro.Text)||string.IsNullOrWhiteSpace(txtCondicion.Text)||string.IsNullOrWhiteSpace(cmbTipo.Text)||string.IsNullOrWhiteSpace(datePlan.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtVaca.Text.Trim(' ').Length != 3||txtToro.Text.Trim(' ').Length != 3)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RP (deben ser 3 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else

            {
                planRepro planRepro = new planRepro();
                planRepro.id_repro = txtIDRepro.Text.Trim();
                planRepro.rp_vaca = txtVaca.Text.Trim();
                planRepro.rp_padre = txtToro.Text.Trim();
                planRepro.condi_corpo = (float)Convert.ToDouble(txtCondicion.Text);
                planRepro.comentario = txtComentario.Text.Trim();
                planRepro.tipo_repro=cmbTipo.Text.Trim();
                planRepro.apli_dispo = chbDispo.IsDisposed;
                planRepro.palpacion= chbPrena.IsDisposed;
                int resultado = planReproAcciones.Agregar(planRepro);
                if (resultado > 0)
                {
                    MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtVaca_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtToro_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private static void SoloDecimales(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtCondicion_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e, false);
        }

        private void txtVaca_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
