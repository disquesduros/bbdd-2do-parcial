﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ConsultaInfoPesajeporAnimal : Form
    {
        public ConsultaInfoPesajeporAnimal()
        {
            InitializeComponent();
        }

        private void ConsultaInfoPesajeporAnimal_Load(object sender, EventArgs e)
        {

        }

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoPesajes pesaje = new IngresoPesajes();
            pesaje.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            pesaje.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                IngresoPesajes animal = new IngresoPesajes();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }
        }

        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                string idPesaje = Convert.ToString(dataGridView1.CurrentRow.Cells[1].Value);
                pesajeAcciones.Eliminar(idPesaje);
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);

            }
            else
                MessageBox.Show("Debe seleccionar una fila");

        }
    }
}
