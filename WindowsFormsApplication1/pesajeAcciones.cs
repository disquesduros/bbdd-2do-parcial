﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class pesajeAcciones
    {
        public static int Agregar(pesaje pesaje)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_pesaje", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("id_pesaje", pesaje.id_pesaje));
            cmd.Parameters.Add(new MySqlParameter("rp_vaca", pesaje.rp_vaca));
            cmd.Parameters.Add(new MySqlParameter("peso", pesaje.peso));
            cmd.Parameters.Add(new MySqlParameter("fecha_pesaje", pesaje.fecha_pesaje));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string id_Pesaje)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_pesaje", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("id_Pesaje", id_Pesaje));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }
    }
}
