﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class IngresoPlanSanitario : Form
    {
        public IngresoPlanSanitario(bool flag)
        {
            InitializeComponent();
            btn_editar.Enabled = false;
            if (flag == true)
            {
                btn_editar.Enabled = flag;
                btn_editar.Visible = true;
                btnguardar.Enabled = false;
                label1.Visible = true;
                txtID.Visible = true;
                linkLabel1.Enabled = false;
                //txtCodigo.Enabled = true;
            }
        }
        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void IngresoPlanSanitario_Load(object sender, EventArgs e)
        {
            List<String> _lista = new List<String>();
            MySqlCommand cmd = new MySqlCommand("buscar_medicamento", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {

                _lista.Add(reader.GetString(0));

            }
            cmbmedicamento.DataSource = _lista;
        }

        private void txtdosis_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten caracteres alfanumericos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoMedicamento med = new WindowsFormsApplication1.IngresoMedicamento();
            
            med.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            med.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtnombre.Text)||string.IsNullOrWhiteSpace(txtrp.Text)||string.IsNullOrWhiteSpace(cmbtipo.Text)||string.IsNullOrWhiteSpace(cmbmedicamento.Text)||string.IsNullOrWhiteSpace(dtfecha.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtrp.Text.Trim(' ').Length < 0 )
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RP (al menos 3 digitos)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else

            {
                planSani planSani = new planSani();
                planSani.fecha_apli = dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
                planSani.id_plan = Convert.ToInt32(txtIDPlan.Text);
                planSani.nombre = txtnombre.Text.Trim();
                planSani.rp_vaca= txtrp.Text.Trim();
                planSani.nombre_med = cmbmedicamento.Text.Trim();
                planSani.dosis = (float)Convert.ToDouble(txtdosis.Text);
                planSani.tipo = cmbtipo.Text.Trim();

                
                int resultado = planSaniAcciones.Agregar(planSani);
                if (resultado > 0)
                {
                    MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private void btndescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbmedicamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void txtrp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtrp_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_editar_Click(object sender, EventArgs e)
        {
            planSani planSani = new planSani();
            planSani.fecha_apli = dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
            planSani.nombre = txtnombre.Text.Trim();
            planSani.rp_vaca = txtrp.Text.Trim();
            planSani.nombre_med = cmbmedicamento.Text.Trim();
            planSani.dosis = (float)Convert.ToDouble(txtdosis.Text);
            planSani.tipo = cmbtipo.Text.Trim();
            planSani.id_plan = Convert.ToInt32(txtID.Text);


            int resultado = planSaniAcciones.Actualizar(planSani);
            if (resultado > 0)
            {
                MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                this.Close();

            }
            else
            {
                MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}