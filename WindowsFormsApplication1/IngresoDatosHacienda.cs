﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoDatosHacienda : Form
    {
        public IngresoDatosHacienda()
        {
            InitializeComponent();
        }
        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        private void IngresoDatosHacienda_Load(object sender, EventArgs e)
        {

        }
        private void txtruc_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }
        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void txtcipropietario_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }
       
        private void btndescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtruc.Text)||string.IsNullOrWhiteSpace(txtnombre.Text)||string.IsNullOrWhiteSpace(txtcipropietario.Text)||string.IsNullOrWhiteSpace(txtemail.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtruc.Text.Trim(' ').Length != 13)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RUC (deben ser 13 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (txtcipropietario.Text.Trim(' ').Length != 10)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo C.I Propietario (deben ser 10 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            this.Close();
        }


        private void txtruc_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
