﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class InformacionBajas : Form
    {
        public InformacionBajas()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void InformacionBajas_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_bajas", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dataGridView1.Rows.Add(true, reader.GetString(0), reader.GetString(5), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
        }

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoBajas bajas = new WindowsFormsApplication1.IngresoBajas();
            bajas.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            bajas.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                WindowsFormsApplication1.IngresoBajas animal = new WindowsFormsApplication1.IngresoBajas();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }
        }
    }
}
