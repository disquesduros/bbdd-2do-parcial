﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoPersonas : Form
    {
        public IngresoPersonas(bool flag)
        {
            InitializeComponent();
            btn_editar.Enabled = false;
            if (flag == true)
            {
                btn_editar.Enabled = flag;
                btn_editar.Visible = true;
                btnguardar.Enabled = false;
                label6.Visible = true;
                txtId.Visible = true;
            }
        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        private void txtcedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void txtdireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b'&& !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten caracteres alfanumericos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void IngresoPersonas_Load(object sender, EventArgs e)
        {

        }
        private void txtmovil_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }
        private void txtconvencional_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtnombre_TextChanged(object sender, EventArgs e)
        {
           
            txtnombre.Text = txtnombre.Text.Replace("  ", " ");
            txtnombre.Select(txtnombre.Text.Length, 0);
        
    }

        private void txtcedula_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtcedula.Text)||string.IsNullOrWhiteSpace(txtnombre.Text)||string.IsNullOrWhiteSpace(txtdireccion.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtcedula.Text.Trim(' ').Length != 10)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo Cedula (deben ser 10 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
           /* else if (txtmovil.Text.Trim(' ').Length != 10)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo Movil (deben ser 10 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }*/
            else

            {
                persona persona = new persona();
                persona.ced = txtcedula.Text.Trim();
                persona.nombre = txtnombre.Text.Trim();
                persona.rol = textrol.Text.Trim();
                persona.dir = txtdireccion.Text.Trim();
                persona.conven = txtconvencional.Text.Trim();
                persona.movil = txtmovil.Text.Trim();
                int resultado = personaAcciones.Agregar(persona);
                if (resultado > 0)
                {
                    MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private void btndescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_editar_Click(object sender, EventArgs e)
        {
            persona persona = new persona();
            persona.ced = txtcedula.Text.Trim();
            persona.nombre = txtnombre.Text.Trim();
            persona.dir = txtdireccion.Text.Trim();
            persona.conven = txtconvencional.Text.Trim();
            persona.movil = txtmovil.Text.Trim();
            persona.cod = Convert.ToInt32(txtId.Text);

            int resultado = personaAcciones.Actualizar(persona);
            if (resultado > 0)
            {
                MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            else
            {
                MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
