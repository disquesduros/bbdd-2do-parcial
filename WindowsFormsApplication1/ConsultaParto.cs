﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ConsInfoParto : Form
    {
        public ConsInfoParto()
        {
            InitializeComponent();
    
        }

        private void ConsInfoParto_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblSelec_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                IngresoParto animal = new IngresoParto();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoParto parto = new IngresoParto();
            parto.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            parto.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                string id_Parto = Convert.ToString(dataGridView1.CurrentRow.Cells[1].Value);
                partoAcciones.Eliminar(id_Parto);
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);

            }
            else
                MessageBox.Show("Debe seleccionar una fila");
        }
    }
}
