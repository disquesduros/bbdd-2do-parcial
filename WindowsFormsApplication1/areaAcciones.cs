﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class areaAcciones
    {
        public static int Agregar(area area)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_area", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("cod", null));
            cmd.Parameters.Add(new MySqlParameter("nom", area.nombre));
            cmd.Parameters.Add(new MySqlParameter("dir", area.dir));
            cmd.Parameters.Add(new MySqlParameter("ubi", area.ubi));
            cmd.Parameters.Add(new MySqlParameter("dim", area.dim));
            cmd.Parameters.Add(new MySqlParameter("capMax", area.capMax));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string nom)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_area", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("nom",nom));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;

        }

        public static int Actualizar(area area )
        {
            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("update_area", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("cod", area.cod));
            cmd.Parameters.Add(new MySqlParameter("nom", area.nombre));
            cmd.Parameters.Add(new MySqlParameter("dir", area.dir));
            cmd.Parameters.Add(new MySqlParameter("ubi", area.ubi));
            cmd.Parameters.Add(new MySqlParameter("dim", area.dim));
            cmd.Parameters.Add(new MySqlParameter("capMax", area.capMax));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }
    }
}
