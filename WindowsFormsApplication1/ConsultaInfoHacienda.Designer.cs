﻿namespace WindowsFormsApplication1
{
    partial class ConsultaInfoHacienda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDatosGenerales = new System.Windows.Forms.Label();
            this.lnkEditar = new System.Windows.Forms.LinkLabel();
            this.lblRUC = new System.Windows.Forms.Label();
            this.lblPropietario = new System.Windows.Forms.Label();
            this.lblCIPropietario = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblAreas = new System.Windows.Forms.Label();
            this.dvgArea = new System.Windows.Forms.DataGridView();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Hectareas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CapacidadMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UbicacionGeo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPersonal = new System.Windows.Forms.Label();
            this.lnkEliminarArea = new System.Windows.Forms.LinkLabel();
            this.lnkNuevoArea = new System.Windows.Forms.LinkLabel();
            this.dgvPersona = new System.Windows.Forms.DataGridView();
            this.SeleccionPersonal = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombrePersona = new System.Windows.Forms.DataGridViewLinkColumn();
            this.DirPersona = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelMovil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelCasa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lnkEliminarPersonal = new System.Windows.Forms.LinkLabel();
            this.lnkNuevoPersonal = new System.Windows.Forms.LinkLabel();
            this.btnVolver = new System.Windows.Forms.Button();
            this.txtRUC = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCI = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dvgArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersona)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDatosGenerales
            // 
            this.lblDatosGenerales.AutoSize = true;
            this.lblDatosGenerales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatosGenerales.Location = new System.Drawing.Point(49, 37);
            this.lblDatosGenerales.Name = "lblDatosGenerales";
            this.lblDatosGenerales.Size = new System.Drawing.Size(114, 15);
            this.lblDatosGenerales.TabIndex = 0;
            this.lblDatosGenerales.Text = "Datos Generales";
            // 
            // lnkEditar
            // 
            this.lnkEditar.AutoSize = true;
            this.lnkEditar.Location = new System.Drawing.Point(191, 38);
            this.lnkEditar.Name = "lnkEditar";
            this.lnkEditar.Size = new System.Drawing.Size(34, 13);
            this.lnkEditar.TabIndex = 1;
            this.lnkEditar.TabStop = true;
            this.lnkEditar.Text = "Editar";
            this.lnkEditar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEditar_LinkClicked);
            // 
            // lblRUC
            // 
            this.lblRUC.AutoSize = true;
            this.lblRUC.BackColor = System.Drawing.SystemColors.Control;
            this.lblRUC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRUC.Location = new System.Drawing.Point(87, 73);
            this.lblRUC.Name = "lblRUC";
            this.lblRUC.Size = new System.Drawing.Size(33, 13);
            this.lblRUC.TabIndex = 2;
            this.lblRUC.Text = "RUC:";
            // 
            // lblPropietario
            // 
            this.lblPropietario.AutoSize = true;
            this.lblPropietario.Location = new System.Drawing.Point(87, 105);
            this.lblPropietario.Name = "lblPropietario";
            this.lblPropietario.Size = new System.Drawing.Size(47, 13);
            this.lblPropietario.TabIndex = 3;
            this.lblPropietario.Text = "Nombre:";
            // 
            // lblCIPropietario
            // 
            this.lblCIPropietario.AutoSize = true;
            this.lblCIPropietario.Location = new System.Drawing.Point(87, 136);
            this.lblCIPropietario.Name = "lblCIPropietario";
            this.lblCIPropietario.Size = new System.Drawing.Size(79, 13);
            this.lblCIPropietario.TabIndex = 4;
            this.lblCIPropietario.Text = "C.I. Propietario:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(87, 167);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 5;
            this.lblEmail.Text = "Email:";
            // 
            // lblAreas
            // 
            this.lblAreas.AutoSize = true;
            this.lblAreas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAreas.Location = new System.Drawing.Point(52, 203);
            this.lblAreas.Name = "lblAreas";
            this.lblAreas.Size = new System.Drawing.Size(43, 15);
            this.lblAreas.TabIndex = 6;
            this.lblAreas.Text = "Áreas";
            // 
            // dvgArea
            // 
            this.dvgArea.AllowUserToAddRows = false;
            this.dvgArea.AllowUserToDeleteRows = false;
            this.dvgArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgArea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.Nombre,
            this.Hectareas,
            this.CapacidadMax,
            this.Direccion,
            this.UbicacionGeo});
            this.dvgArea.Location = new System.Drawing.Point(68, 239);
            this.dvgArea.Name = "dvgArea";
            this.dvgArea.ReadOnly = true;
            this.dvgArea.Size = new System.Drawing.Size(639, 102);
            this.dvgArea.TabIndex = 7;
            this.dvgArea.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgArea_CellContentClick);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Seleccionar.Width = 70;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Nombre.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Hectareas
            // 
            this.Hectareas.HeaderText = "Hectáreas de Tierra";
            this.Hectareas.Name = "Hectareas";
            // 
            // CapacidadMax
            // 
            this.CapacidadMax.HeaderText = "Capacidad Máxima";
            this.CapacidadMax.Name = "CapacidadMax";
            // 
            // Direccion
            // 
            this.Direccion.HeaderText = "Dirección";
            this.Direccion.Name = "Direccion";
            this.Direccion.Width = 130;
            // 
            // UbicacionGeo
            // 
            this.UbicacionGeo.HeaderText = "Ubicación Geográfica";
            this.UbicacionGeo.Name = "UbicacionGeo";
            // 
            // lblPersonal
            // 
            this.lblPersonal.AutoSize = true;
            this.lblPersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonal.Location = new System.Drawing.Point(52, 379);
            this.lblPersonal.Name = "lblPersonal";
            this.lblPersonal.Size = new System.Drawing.Size(64, 15);
            this.lblPersonal.TabIndex = 8;
            this.lblPersonal.Text = "Personal";
            // 
            // lnkEliminarArea
            // 
            this.lnkEliminarArea.AutoSize = true;
            this.lnkEliminarArea.Location = new System.Drawing.Point(68, 348);
            this.lnkEliminarArea.Name = "lnkEliminarArea";
            this.lnkEliminarArea.Size = new System.Drawing.Size(55, 13);
            this.lnkEliminarArea.TabIndex = 9;
            this.lnkEliminarArea.TabStop = true;
            this.lnkEliminarArea.Text = "Eliminar (-)";
            this.lnkEliminarArea.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEliminarArea_LinkClicked);
            // 
            // lnkNuevoArea
            // 
            this.lnkNuevoArea.AutoSize = true;
            this.lnkNuevoArea.Location = new System.Drawing.Point(653, 348);
            this.lnkNuevoArea.Name = "lnkNuevoArea";
            this.lnkNuevoArea.Size = new System.Drawing.Size(54, 13);
            this.lnkNuevoArea.TabIndex = 10;
            this.lnkNuevoArea.TabStop = true;
            this.lnkNuevoArea.Text = "Nuevo (+)";
            this.lnkNuevoArea.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNuevoArea_LinkClicked);
            // 
            // dgvPersona
            // 
            this.dgvPersona.AllowUserToAddRows = false;
            this.dgvPersona.AllowUserToDeleteRows = false;
            this.dgvPersona.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersona.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SeleccionPersonal,
            this.Cedula,
            this.NombrePersona,
            this.DirPersona,
            this.TelMovil,
            this.TelCasa});
            this.dgvPersona.Location = new System.Drawing.Point(68, 410);
            this.dgvPersona.Name = "dgvPersona";
            this.dgvPersona.Size = new System.Drawing.Size(639, 97);
            this.dgvPersona.TabIndex = 11;
            this.dgvPersona.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPersona_CellContentClick);
            // 
            // SeleccionPersonal
            // 
            this.SeleccionPersonal.HeaderText = "Seleccionar";
            this.SeleccionPersonal.Name = "SeleccionPersonal";
            this.SeleccionPersonal.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SeleccionPersonal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SeleccionPersonal.Width = 70;
            // 
            // Cedula
            // 
            this.Cedula.HeaderText = "Cédula";
            this.Cedula.Name = "Cedula";
            this.Cedula.ReadOnly = true;
            // 
            // NombrePersona
            // 
            this.NombrePersona.HeaderText = "Nombre";
            this.NombrePersona.Name = "NombrePersona";
            this.NombrePersona.ReadOnly = true;
            this.NombrePersona.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NombrePersona.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DirPersona
            // 
            this.DirPersona.HeaderText = "Dirección";
            this.DirPersona.Name = "DirPersona";
            this.DirPersona.ReadOnly = true;
            this.DirPersona.Width = 130;
            // 
            // TelMovil
            // 
            this.TelMovil.HeaderText = "Teléfono Móvil";
            this.TelMovil.Name = "TelMovil";
            this.TelMovil.ReadOnly = true;
            // 
            // TelCasa
            // 
            this.TelCasa.HeaderText = "Teléfono Casa";
            this.TelCasa.Name = "TelCasa";
            this.TelCasa.ReadOnly = true;
            // 
            // lnkEliminarPersonal
            // 
            this.lnkEliminarPersonal.AutoSize = true;
            this.lnkEliminarPersonal.Location = new System.Drawing.Point(68, 514);
            this.lnkEliminarPersonal.Name = "lnkEliminarPersonal";
            this.lnkEliminarPersonal.Size = new System.Drawing.Size(55, 13);
            this.lnkEliminarPersonal.TabIndex = 12;
            this.lnkEliminarPersonal.TabStop = true;
            this.lnkEliminarPersonal.Text = "Eliminar (-)";
            this.lnkEliminarPersonal.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEliminarPersonal_LinkClicked);
            // 
            // lnkNuevoPersonal
            // 
            this.lnkNuevoPersonal.AutoSize = true;
            this.lnkNuevoPersonal.Location = new System.Drawing.Point(653, 514);
            this.lnkNuevoPersonal.Name = "lnkNuevoPersonal";
            this.lnkNuevoPersonal.Size = new System.Drawing.Size(54, 13);
            this.lnkNuevoPersonal.TabIndex = 13;
            this.lnkNuevoPersonal.TabStop = true;
            this.lnkNuevoPersonal.Text = "Nuevo (+)";
            this.lnkNuevoPersonal.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNuevoPersonal_LinkClicked);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(319, 556);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(114, 38);
            this.btnVolver.TabIndex = 14;
            this.btnVolver.Text = "VOLVER";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // txtRUC
            // 
            this.txtRUC.Location = new System.Drawing.Point(182, 70);
            this.txtRUC.Name = "txtRUC";
            this.txtRUC.ReadOnly = true;
            this.txtRUC.Size = new System.Drawing.Size(136, 20);
            this.txtRUC.TabIndex = 15;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(182, 102);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(136, 20);
            this.txtNombre.TabIndex = 16;
            // 
            // txtCI
            // 
            this.txtCI.Location = new System.Drawing.Point(182, 133);
            this.txtCI.Name = "txtCI";
            this.txtCI.ReadOnly = true;
            this.txtCI.Size = new System.Drawing.Size(136, 20);
            this.txtCI.TabIndex = 17;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(182, 164);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(215, 20);
            this.txtEmail.TabIndex = 18;
            // 
            // ConsultaInfoHacienda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 606);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtCI);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtRUC);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.lnkNuevoPersonal);
            this.Controls.Add(this.lnkEliminarPersonal);
            this.Controls.Add(this.dgvPersona);
            this.Controls.Add(this.lnkNuevoArea);
            this.Controls.Add(this.lnkEliminarArea);
            this.Controls.Add(this.lblPersonal);
            this.Controls.Add(this.dvgArea);
            this.Controls.Add(this.lblAreas);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblCIPropietario);
            this.Controls.Add(this.lblPropietario);
            this.Controls.Add(this.lblRUC);
            this.Controls.Add(this.lnkEditar);
            this.Controls.Add(this.lblDatosGenerales);
            this.Name = "ConsultaInfoHacienda";
            this.Text = "Consulta Información de la Hacienda";
            this.Load += new System.EventHandler(this.ConsultaInfoHacienda_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dvgArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersona)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDatosGenerales;
        private System.Windows.Forms.LinkLabel lnkEditar;
        private System.Windows.Forms.Label lblRUC;
        private System.Windows.Forms.Label lblPropietario;
        private System.Windows.Forms.Label lblCIPropietario;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblAreas;
        private System.Windows.Forms.Label lblPersonal;
        private System.Windows.Forms.LinkLabel lnkEliminarArea;
        private System.Windows.Forms.LinkLabel lnkNuevoArea;
        private System.Windows.Forms.LinkLabel lnkEliminarPersonal;
        private System.Windows.Forms.LinkLabel lnkNuevoPersonal;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.TextBox txtRUC;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCI;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewLinkColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hectareas;
        private System.Windows.Forms.DataGridViewTextBoxColumn CapacidadMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn UbicacionGeo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SeleccionPersonal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cedula;
        private System.Windows.Forms.DataGridViewLinkColumn NombrePersona;
        private System.Windows.Forms.DataGridViewTextBoxColumn DirPersona;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelMovil;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelCasa;
        public System.Windows.Forms.DataGridView dvgArea;
        public System.Windows.Forms.DataGridView dgvPersona;
    }
}