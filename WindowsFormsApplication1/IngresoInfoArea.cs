﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class IngresoInfoArea : Form
    {
        public IngresoInfoArea(bool flag)
        {
            InitializeComponent();
            btn_editar.Enabled = false;
            if (flag == true) {
                btn_editar.Enabled = flag;
                btnGuardar.Enabled = false;
                label1.Visible = true;
                txtCodigo.Visible = true;
                //txtCodigo.Enabled = true;
            }
            
        }

        private void IngresoInfoArea_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text)||string.IsNullOrWhiteSpace(txtDimensiones.Text)||string.IsNullOrWhiteSpace(txtDireccion.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else

            {
                area area = new area();
                area.cod = txtcod.Text.Trim();
                area.nombre = txtNombre.Text.Trim();
                area.dim = (float)Convert.ToDouble(txtDimensiones.Text);
                area.ubi = txtUbicacion.Text.Trim();
                area.capMax = txtCapacidadMax.Text.Trim();
                area.dir = txtDireccion.Text.Trim();
                int resultado = areaAcciones.Agregar(area);
                if (resultado > 0)
                {
                    MessageBox.Show("Cliente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private static void SoloNumeros(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtDimensiones_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e, false);
        }

        private void txtCapacidadMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }

        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDimensiones_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_editar_Click(object sender, EventArgs e)
        {

            area area = new area();
            area.cod = Convert.ToInt32(txtCodigo.Text);
            area.nombre = txtNombre.Text.Trim();
            area.dim = (float)Convert.ToDouble(txtDimensiones.Text);
            area.ubi = txtUbicacion.Text.Trim();
            area.capMax = txtCapacidadMax.Text.Trim();
            area.dir = txtDireccion.Text.Trim();
            

            int resultado = areaAcciones.Actualizar(area);
            if (resultado > 0)
            {
                MessageBox.Show("Cliente editado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                this.Close();

            }
            else
            {
                MessageBox.Show("No se pudo editar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
