﻿namespace WindowsFormsApplication1
{
    partial class IngresoInfoAnimalParido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRP = new System.Windows.Forms.Label();
            this.lblRGD = new System.Windows.Forms.Label();
            this.lblPropietario = new System.Windows.Forms.Label();
            this.lblRPPadre = new System.Windows.Forms.Label();
            this.lblRPMadre = new System.Windows.Forms.Label();
            this.lblRGDPadre = new System.Windows.Forms.Label();
            this.lblRaza = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblNoArete = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtRGD = new System.Windows.Forms.TextBox();
            this.txtMadre = new System.Windows.Forms.TextBox();
            this.txtArete = new System.Windows.Forms.TextBox();
            this.txtPadre = new System.Windows.Forms.TextBox();
            this.txtRGDPadre = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.cmbUbicacion = new System.Windows.Forms.ComboBox();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.cmbRaza = new System.Windows.Forms.ComboBox();
            this.cmbPropietario = new System.Windows.Forms.ComboBox();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.cmbSexo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRP.Location = new System.Drawing.Point(74, 33);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(27, 17);
            this.lblRP.TabIndex = 1;
            this.lblRP.Text = "RP";
            // 
            // lblRGD
            // 
            this.lblRGD.AutoSize = true;
            this.lblRGD.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRGD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRGD.Location = new System.Drawing.Point(74, 122);
            this.lblRGD.Name = "lblRGD";
            this.lblRGD.Size = new System.Drawing.Size(39, 17);
            this.lblRGD.TabIndex = 2;
            this.lblRGD.Text = "RGD";
            // 
            // lblPropietario
            // 
            this.lblPropietario.AutoSize = true;
            this.lblPropietario.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPropietario.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPropietario.Location = new System.Drawing.Point(74, 150);
            this.lblPropietario.Name = "lblPropietario";
            this.lblPropietario.Size = new System.Drawing.Size(77, 17);
            this.lblPropietario.TabIndex = 3;
            this.lblPropietario.Text = "Propietario";
            // 
            // lblRPPadre
            // 
            this.lblRPPadre.AutoSize = true;
            this.lblRPPadre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRPPadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRPPadre.Location = new System.Drawing.Point(282, 150);
            this.lblRPPadre.Name = "lblRPPadre";
            this.lblRPPadre.Size = new System.Drawing.Size(69, 17);
            this.lblRPPadre.TabIndex = 4;
            this.lblRPPadre.Text = "RP Padre";
            // 
            // lblRPMadre
            // 
            this.lblRPMadre.AutoSize = true;
            this.lblRPMadre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRPMadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRPMadre.Location = new System.Drawing.Point(74, 181);
            this.lblRPMadre.Name = "lblRPMadre";
            this.lblRPMadre.Size = new System.Drawing.Size(71, 17);
            this.lblRPMadre.TabIndex = 5;
            this.lblRPMadre.Text = "RP Madre";
            // 
            // lblRGDPadre
            // 
            this.lblRGDPadre.AutoSize = true;
            this.lblRGDPadre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRGDPadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRGDPadre.Location = new System.Drawing.Point(282, 181);
            this.lblRGDPadre.Name = "lblRGDPadre";
            this.lblRGDPadre.Size = new System.Drawing.Size(81, 17);
            this.lblRGDPadre.TabIndex = 6;
            this.lblRGDPadre.Text = "RGD Padre";
            // 
            // lblRaza
            // 
            this.lblRaza.AutoSize = true;
            this.lblRaza.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblRaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRaza.Location = new System.Drawing.Point(282, 122);
            this.lblRaza.Name = "lblRaza";
            this.lblRaza.Size = new System.Drawing.Size(41, 17);
            this.lblRaza.TabIndex = 7;
            this.lblRaza.Text = "Raza";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(282, 93);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(52, 17);
            this.lblEstado.TabIndex = 8;
            this.lblEstado.Text = "Estado";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Location = new System.Drawing.Point(74, 93);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(39, 17);
            this.lblSexo.TabIndex = 9;
            this.lblSexo.Text = "Sexo";
            // 
            // lblNoArete
            // 
            this.lblNoArete.AutoSize = true;
            this.lblNoArete.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblNoArete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoArete.Location = new System.Drawing.Point(282, 62);
            this.lblNoArete.Name = "lblNoArete";
            this.lblNoArete.Size = new System.Drawing.Size(68, 17);
            this.lblNoArete.TabIndex = 10;
            this.lblNoArete.Text = "No. Arete";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(74, 62);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(58, 17);
            this.lblNombre.TabIndex = 11;
            this.lblNombre.Text = "Nombre";
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.AutoSize = true;
            this.lblUbicacion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblUbicacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUbicacion.Location = new System.Drawing.Point(282, 33);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(70, 17);
            this.lblUbicacion.TabIndex = 12;
            this.lblUbicacion.Text = "Ubicacion";
            // 
            // txtRP
            // 
            this.txtRP.Location = new System.Drawing.Point(157, 32);
            this.txtRP.MaxLength = 3;
            this.txtRP.Name = "txtRP";
            this.txtRP.Size = new System.Drawing.Size(100, 20);
            this.txtRP.TabIndex = 13;
            this.txtRP.TextChanged += new System.EventHandler(this.txtRP_TextChanged);
            this.txtRP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRP_KeyPress);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(157, 59);
            this.txtNombre.MaxLength = 10;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 14;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // txtRGD
            // 
            this.txtRGD.Location = new System.Drawing.Point(157, 121);
            this.txtRGD.MaxLength = 10;
            this.txtRGD.Name = "txtRGD";
            this.txtRGD.Size = new System.Drawing.Size(100, 20);
            this.txtRGD.TabIndex = 16;
            this.txtRGD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRDG_KeyPress);
            // 
            // txtMadre
            // 
            this.txtMadre.Location = new System.Drawing.Point(157, 180);
            this.txtMadre.MaxLength = 3;
            this.txtMadre.Name = "txtMadre";
            this.txtMadre.Size = new System.Drawing.Size(100, 20);
            this.txtMadre.TabIndex = 18;
            this.txtMadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMadre_KeyPress);
            // 
            // txtArete
            // 
            this.txtArete.Location = new System.Drawing.Point(369, 59);
            this.txtArete.MaxLength = 4;
            this.txtArete.Name = "txtArete";
            this.txtArete.Size = new System.Drawing.Size(100, 20);
            this.txtArete.TabIndex = 20;
            this.txtArete.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtArete_KeyPress);
            // 
            // txtPadre
            // 
            this.txtPadre.Location = new System.Drawing.Point(369, 147);
            this.txtPadre.MaxLength = 3;
            this.txtPadre.Name = "txtPadre";
            this.txtPadre.Size = new System.Drawing.Size(100, 20);
            this.txtPadre.TabIndex = 23;
            this.txtPadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPadre_KeyPress);
            // 
            // txtRGDPadre
            // 
            this.txtRGDPadre.Location = new System.Drawing.Point(369, 181);
            this.txtRGDPadre.MaxLength = 10;
            this.txtRGDPadre.Name = "txtRGDPadre";
            this.txtRGDPadre.Size = new System.Drawing.Size(100, 20);
            this.txtRGDPadre.TabIndex = 24;
            this.txtRGDPadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRGDPadre_KeyPress);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(157, 231);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(102, 23);
            this.btnGuardar.TabIndex = 25;
            this.btnGuardar.Text = "Guardar/Cerrar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // cmbUbicacion
            // 
            this.cmbUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUbicacion.FormattingEnabled = true;
            this.cmbUbicacion.Items.AddRange(new object[] {
            "San Fernando",
            "La Laguna"});
            this.cmbUbicacion.Location = new System.Drawing.Point(369, 32);
            this.cmbUbicacion.Name = "cmbUbicacion";
            this.cmbUbicacion.Size = new System.Drawing.Size(100, 21);
            this.cmbUbicacion.TabIndex = 26;
            // 
            // cmbEstado
            // 
            this.cmbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.cmbEstado.Location = new System.Drawing.Point(369, 92);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(100, 21);
            this.cmbEstado.TabIndex = 27;
            // 
            // cmbRaza
            // 
            this.cmbRaza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRaza.FormattingEnabled = true;
            this.cmbRaza.Items.AddRange(new object[] {
            "Nelore"});
            this.cmbRaza.Location = new System.Drawing.Point(369, 121);
            this.cmbRaza.Name = "cmbRaza";
            this.cmbRaza.Size = new System.Drawing.Size(100, 21);
            this.cmbRaza.TabIndex = 28;
            // 
            // cmbPropietario
            // 
            this.cmbPropietario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPropietario.FormattingEnabled = true;
            this.cmbPropietario.Location = new System.Drawing.Point(157, 147);
            this.cmbPropietario.Name = "cmbPropietario";
            this.cmbPropietario.Size = new System.Drawing.Size(100, 21);
            this.cmbPropietario.TabIndex = 29;
            // 
            // btnDescartar
            // 
            this.btnDescartar.Location = new System.Drawing.Point(338, 231);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(102, 23);
            this.btnDescartar.TabIndex = 30;
            this.btnDescartar.Text = "Descartar";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            // 
            // cmbSexo
            // 
            this.cmbSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSexo.FormattingEnabled = true;
            this.cmbSexo.Items.AddRange(new object[] {
            "HEMBRA",
            "MACHO"});
            this.cmbSexo.Location = new System.Drawing.Point(155, 94);
            this.cmbSexo.Name = "cmbSexo";
            this.cmbSexo.Size = new System.Drawing.Size(104, 21);
            this.cmbSexo.TabIndex = 31;
            // 
            // IngresoInfoAnimalParido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 311);
            this.Controls.Add(this.cmbSexo);
            this.Controls.Add(this.btnDescartar);
            this.Controls.Add(this.cmbPropietario);
            this.Controls.Add(this.cmbRaza);
            this.Controls.Add(this.cmbEstado);
            this.Controls.Add(this.cmbUbicacion);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtRGDPadre);
            this.Controls.Add(this.txtPadre);
            this.Controls.Add(this.txtArete);
            this.Controls.Add(this.txtMadre);
            this.Controls.Add(this.txtRGD);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.lblUbicacion);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblNoArete);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.lblRaza);
            this.Controls.Add(this.lblRGDPadre);
            this.Controls.Add(this.lblRPMadre);
            this.Controls.Add(this.lblRPPadre);
            this.Controls.Add(this.lblPropietario);
            this.Controls.Add(this.lblRGD);
            this.Controls.Add(this.lblRP);
            this.Name = "IngresoInfoAnimalParido";
            this.Text = "Ingreso de Información del Animal Parido";
            this.Load += new System.EventHandler(this.IngresoInfoAnimalParido_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRP;
        private System.Windows.Forms.Label lblRGD;
        private System.Windows.Forms.Label lblPropietario;
        private System.Windows.Forms.Label lblRPPadre;
        private System.Windows.Forms.Label lblRPMadre;
        private System.Windows.Forms.Label lblRGDPadre;
        private System.Windows.Forms.Label lblRaza;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblNoArete;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtRGD;
        private System.Windows.Forms.TextBox txtMadre;
        private System.Windows.Forms.TextBox txtArete;
        private System.Windows.Forms.TextBox txtPadre;
        private System.Windows.Forms.TextBox txtRGDPadre;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ComboBox cmbUbicacion;
        private System.Windows.Forms.ComboBox cmbEstado;
        private System.Windows.Forms.ComboBox cmbRaza;
        private System.Windows.Forms.ComboBox cmbPropietario;
        private System.Windows.Forms.Button btnDescartar;
        private System.Windows.Forms.ComboBox cmbSexo;
    }
}