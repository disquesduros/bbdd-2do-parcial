﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class area
    {
        public int cod { get; set; }
        public string nombre { get; set; }
        public string dir { get; set; }
        public string ubi { get; set; }
        public float dim { get; set; }
        public string capMax{ get; set; }
        public area() { }

        public area(int cod, string nombre, string dir, string ubi, float dim, string capMax)
        {
            this.cod = cod;
            this.nombre = nombre;
            this.dir = dir;
            this.ubi = ubi;
            this.dim = dim;
            this.capMax = capMax;

        }
    }
}
