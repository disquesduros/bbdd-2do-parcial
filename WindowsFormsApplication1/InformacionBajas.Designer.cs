﻿namespace WindowsFormsApplication1
{
    partial class InformacionBajas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InformacionBajas));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.rdtipo = new System.Windows.Forms.RadioButton();
            this.rdpropietario = new System.Windows.Forms.RadioButton();
            this.rdcriterio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rdnombre = new System.Windows.Forms.RadioButton();
            this.rdrp = new System.Windows.Forms.RadioButton();
            this.btnvolver = new System.Windows.Forms.Button();
            this.lblEliminar = new System.Windows.Forms.LinkLabel();
            this.lblNuevo = new System.Windows.Forms.LinkLabel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblFechaHasta = new System.Windows.Forms.Label();
            this.lblFechaDesde = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IDPesaje = new System.Windows.Forms.DataGridViewLinkColumn();
            this.RpAnimal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaBaja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoBaja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Peso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PesoLibra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnbuscar);
            this.groupBox1.Controls.Add(this.rdtipo);
            this.groupBox1.Controls.Add(this.rdpropietario);
            this.groupBox1.Controls.Add(this.rdcriterio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rdnombre);
            this.groupBox1.Controls.Add(this.rdrp);
            this.groupBox1.Location = new System.Drawing.Point(63, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(586, 97);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Busqueda";
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(505, 32);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(75, 23);
            this.btnbuscar.TabIndex = 19;
            this.btnbuscar.Text = "Buscar";
            this.btnbuscar.UseVisualStyleBackColor = true;
            this.btnbuscar.Click += new System.EventHandler(this.button2_Click);
            // 
            // rdtipo
            // 
            this.rdtipo.AutoSize = true;
            this.rdtipo.Location = new System.Drawing.Point(318, 20);
            this.rdtipo.Name = "rdtipo";
            this.rdtipo.Size = new System.Drawing.Size(46, 17);
            this.rdtipo.TabIndex = 3;
            this.rdtipo.TabStop = true;
            this.rdtipo.Text = "Tipo";
            this.rdtipo.UseVisualStyleBackColor = true;
            // 
            // rdpropietario
            // 
            this.rdpropietario.AutoSize = true;
            this.rdpropietario.Location = new System.Drawing.Point(209, 20);
            this.rdpropietario.Name = "rdpropietario";
            this.rdpropietario.Size = new System.Drawing.Size(75, 17);
            this.rdpropietario.TabIndex = 2;
            this.rdpropietario.TabStop = true;
            this.rdpropietario.Text = "Propietario";
            this.rdpropietario.UseVisualStyleBackColor = true;
            // 
            // rdcriterio
            // 
            this.rdcriterio.Location = new System.Drawing.Point(128, 61);
            this.rdcriterio.Name = "rdcriterio";
            this.rdcriterio.Size = new System.Drawing.Size(369, 20);
            this.rdcriterio.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Criterio de busqueda:";
            // 
            // rdnombre
            // 
            this.rdnombre.AutoSize = true;
            this.rdnombre.Location = new System.Drawing.Point(109, 20);
            this.rdnombre.Name = "rdnombre";
            this.rdnombre.Size = new System.Drawing.Size(62, 17);
            this.rdnombre.TabIndex = 1;
            this.rdnombre.TabStop = true;
            this.rdnombre.Text = "Nombre";
            this.rdnombre.UseVisualStyleBackColor = true;
            // 
            // rdrp
            // 
            this.rdrp.AutoSize = true;
            this.rdrp.Location = new System.Drawing.Point(18, 20);
            this.rdrp.Name = "rdrp";
            this.rdrp.Size = new System.Drawing.Size(40, 17);
            this.rdrp.TabIndex = 0;
            this.rdrp.TabStop = true;
            this.rdrp.Text = "RP";
            this.rdrp.UseVisualStyleBackColor = true;
            // 
            // btnvolver
            // 
            this.btnvolver.Location = new System.Drawing.Point(336, 400);
            this.btnvolver.Name = "btnvolver";
            this.btnvolver.Size = new System.Drawing.Size(75, 23);
            this.btnvolver.TabIndex = 13;
            this.btnvolver.Text = "Volver";
            this.btnvolver.UseVisualStyleBackColor = true;
            this.btnvolver.Click += new System.EventHandler(this.btnvolver_Click);
            // 
            // lblEliminar
            // 
            this.lblEliminar.AutoSize = true;
            this.lblEliminar.Location = new System.Drawing.Point(12, 333);
            this.lblEliminar.Name = "lblEliminar";
            this.lblEliminar.Size = new System.Drawing.Size(55, 13);
            this.lblEliminar.TabIndex = 17;
            this.lblEliminar.TabStop = true;
            this.lblEliminar.Text = "Eliminar (-)";
            // 
            // lblNuevo
            // 
            this.lblNuevo.AutoSize = true;
            this.lblNuevo.Location = new System.Drawing.Point(696, 333);
            this.lblNuevo.Name = "lblNuevo";
            this.lblNuevo.Size = new System.Drawing.Size(54, 13);
            this.lblNuevo.TabIndex = 18;
            this.lblNuevo.TabStop = true;
            this.lblNuevo.Text = "Nuevo (+)";
            this.lblNuevo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNuevo_LinkClicked);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(539, 143);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 22;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(142, 143);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 21;
            // 
            // lblFechaHasta
            // 
            this.lblFechaHasta.AutoSize = true;
            this.lblFechaHasta.BackColor = System.Drawing.SystemColors.Control;
            this.lblFechaHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaHasta.Location = new System.Drawing.Point(461, 143);
            this.lblFechaHasta.Name = "lblFechaHasta";
            this.lblFechaHasta.Size = new System.Drawing.Size(69, 13);
            this.lblFechaHasta.TabIndex = 20;
            this.lblFechaHasta.Text = "Fecha hasta:";
            // 
            // lblFechaDesde
            // 
            this.lblFechaDesde.AutoSize = true;
            this.lblFechaDesde.BackColor = System.Drawing.SystemColors.Control;
            this.lblFechaDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaDesde.Location = new System.Drawing.Point(60, 143);
            this.lblFechaDesde.Name = "lblFechaDesde";
            this.lblFechaDesde.Size = new System.Drawing.Size(72, 13);
            this.lblFechaDesde.TabIndex = 19;
            this.lblFechaDesde.Text = "Fecha desde:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.IDPesaje,
            this.RpAnimal,
            this.FechaBaja,
            this.TipoBaja,
            this.Peso,
            this.PesoLibra});
            this.dataGridView1.Location = new System.Drawing.Point(34, 180);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(738, 150);
            this.dataGridView1.TabIndex = 23;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.ReadOnly = true;
            this.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IDPesaje
            // 
            this.IDPesaje.HeaderText = "Código";
            this.IDPesaje.Name = "IDPesaje";
            this.IDPesaje.ReadOnly = true;
            // 
            // RpAnimal
            // 
            this.RpAnimal.HeaderText = "RP Animal";
            this.RpAnimal.Name = "RpAnimal";
            this.RpAnimal.ReadOnly = true;
            // 
            // FechaBaja
            // 
            this.FechaBaja.HeaderText = "Fecha de Baja";
            this.FechaBaja.Name = "FechaBaja";
            this.FechaBaja.ReadOnly = true;
            // 
            // TipoBaja
            // 
            this.TipoBaja.HeaderText = "Tipo de Baja";
            this.TipoBaja.Name = "TipoBaja";
            this.TipoBaja.ReadOnly = true;
            // 
            // Peso
            // 
            this.Peso.HeaderText = "Peso";
            this.Peso.Name = "Peso";
            this.Peso.ReadOnly = true;
            // 
            // PesoLibra
            // 
            this.PesoLibra.HeaderText = "Precio por Libra";
            this.PesoLibra.Name = "PesoLibra";
            this.PesoLibra.ReadOnly = true;
            // 
            // InformacionBajas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 435);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.lblFechaHasta);
            this.Controls.Add(this.lblFechaDesde);
            this.Controls.Add(this.lblNuevo);
            this.Controls.Add(this.lblEliminar);
            this.Controls.Add(this.btnvolver);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InformacionBajas";
            this.Text = "Consulta Información de Bajas";
            this.Load += new System.EventHandler(this.InformacionBajas_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdtipo;
        private System.Windows.Forms.RadioButton rdpropietario;
        private System.Windows.Forms.TextBox rdcriterio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdnombre;
        private System.Windows.Forms.RadioButton rdrp;
        private System.Windows.Forms.Button btnvolver;
        private System.Windows.Forms.LinkLabel lblEliminar;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.LinkLabel lblNuevo;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblFechaHasta;
        private System.Windows.Forms.Label lblFechaDesde;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewLinkColumn IDPesaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn RpAnimal;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaBaja;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoBaja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Peso;
        private System.Windows.Forms.DataGridViewTextBoxColumn PesoLibra;
    }
}