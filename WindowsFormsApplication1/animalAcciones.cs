﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class animalAcciones
    {
        public static int Agregar(animal animal)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_animal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp",animal.rp));
            cmd.Parameters.Add(new MySqlParameter("n_arete",animal.n_arete));
            cmd.Parameters.Add(new MySqlParameter("id_area",animal.id_area));
            cmd.Parameters.Add(new MySqlParameter("nom",animal.nombre));
            cmd.Parameters.Add(new MySqlParameter("sexo",animal.sexo));
            cmd.Parameters.Add(new MySqlParameter("fecha_llega",animal.fecha_llega));
            cmd.Parameters.Add(new MySqlParameter("haci_ori", animal.haci_ori));
            cmd.Parameters.Add(new MySqlParameter("propi", animal.propi));
            cmd.Parameters.Add(new MySqlParameter("rp_madre", animal.rp_madre));
            cmd.Parameters.Add(new MySqlParameter("rp_padre", animal.rp_padre));
            cmd.Parameters.Add(new MySqlParameter("estado", animal.estado));
            cmd.Parameters.Add(new MySqlParameter("raza", animal.raza));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string inRp)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_animal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("inRp", inRp));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }
    }
}
