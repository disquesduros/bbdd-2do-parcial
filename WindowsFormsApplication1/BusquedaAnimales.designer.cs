﻿namespace WindowsFormsApplication1
{
    partial class BusquedaAnimales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BusquedaAnimales));
            this.dgvbusquedaanimal = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdubicacion = new System.Windows.Forms.RadioButton();
            this.rdpropietario = new System.Windows.Forms.RadioButton();
            this.rdnombre = new System.Windows.Forms.RadioButton();
            this.rdrp = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtcriterio = new System.Windows.Forms.TextBox();
            this.btnvolver = new System.Windows.Forms.Button();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.lnkEliminar = new System.Windows.Forms.LinkLabel();
            this.lnkNuevo = new System.Windows.Forms.LinkLabel();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RP = new System.Windows.Forms.DataGridViewLinkColumn();
            this.UbicacionActual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Propietario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreAnimal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sexo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaNacimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoPartos = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Estado = new System.Windows.Forms.DataGridViewLinkColumn();
            this.UltimoPeso = new System.Windows.Forms.DataGridViewLinkColumn();
            this.UltimoPlanSanitario = new System.Windows.Forms.DataGridViewLinkColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbusquedaanimal)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvbusquedaanimal
            // 
            this.dgvbusquedaanimal.AllowUserToAddRows = false;
            this.dgvbusquedaanimal.AllowUserToDeleteRows = false;
            this.dgvbusquedaanimal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvbusquedaanimal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.RP,
            this.UbicacionActual,
            this.Propietario,
            this.NombreAnimal,
            this.Sexo,
            this.FechaNacimiento,
            this.NoPartos,
            this.Estado,
            this.UltimoPeso,
            this.UltimoPlanSanitario});
            this.dgvbusquedaanimal.Location = new System.Drawing.Point(26, 121);
            this.dgvbusquedaanimal.Name = "dgvbusquedaanimal";
            this.dgvbusquedaanimal.Size = new System.Drawing.Size(1013, 86);
            this.dgvbusquedaanimal.TabIndex = 13;
            this.dgvbusquedaanimal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvbusquedaanimal_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdubicacion);
            this.groupBox1.Controls.Add(this.rdpropietario);
            this.groupBox1.Controls.Add(this.rdnombre);
            this.groupBox1.Controls.Add(this.rdrp);
            this.groupBox1.Location = new System.Drawing.Point(128, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(490, 44);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione";
            // 
            // rdubicacion
            // 
            this.rdubicacion.AutoSize = true;
            this.rdubicacion.Location = new System.Drawing.Point(318, 20);
            this.rdubicacion.Name = "rdubicacion";
            this.rdubicacion.Size = new System.Drawing.Size(105, 17);
            this.rdubicacion.TabIndex = 3;
            this.rdubicacion.TabStop = true;
            this.rdubicacion.Text = "Ubicación actual";
            this.rdubicacion.UseVisualStyleBackColor = true;
            // 
            // rdpropietario
            // 
            this.rdpropietario.AutoSize = true;
            this.rdpropietario.Location = new System.Drawing.Point(209, 20);
            this.rdpropietario.Name = "rdpropietario";
            this.rdpropietario.Size = new System.Drawing.Size(75, 17);
            this.rdpropietario.TabIndex = 2;
            this.rdpropietario.TabStop = true;
            this.rdpropietario.Text = "Propietario";
            this.rdpropietario.UseVisualStyleBackColor = true;
            // 
            // rdnombre
            // 
            this.rdnombre.AutoSize = true;
            this.rdnombre.Location = new System.Drawing.Point(109, 20);
            this.rdnombre.Name = "rdnombre";
            this.rdnombre.Size = new System.Drawing.Size(62, 17);
            this.rdnombre.TabIndex = 1;
            this.rdnombre.TabStop = true;
            this.rdnombre.Text = "Nombre";
            this.rdnombre.UseVisualStyleBackColor = true;
            // 
            // rdrp
            // 
            this.rdrp.AutoSize = true;
            this.rdrp.Location = new System.Drawing.Point(18, 20);
            this.rdrp.Name = "rdrp";
            this.rdrp.Size = new System.Drawing.Size(40, 17);
            this.rdrp.TabIndex = 0;
            this.rdrp.TabStop = true;
            this.rdrp.Text = "RP";
            this.rdrp.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Criterio de busqueda:";
            // 
            // txtcriterio
            // 
            this.txtcriterio.Location = new System.Drawing.Point(220, 74);
            this.txtcriterio.Name = "txtcriterio";
            this.txtcriterio.Size = new System.Drawing.Size(369, 20);
            this.txtcriterio.TabIndex = 9;
            // 
            // btnvolver
            // 
            this.btnvolver.Location = new System.Drawing.Point(543, 267);
            this.btnvolver.Name = "btnvolver";
            this.btnvolver.Size = new System.Drawing.Size(75, 33);
            this.btnvolver.TabIndex = 8;
            this.btnvolver.Text = "Volver";
            this.btnvolver.UseVisualStyleBackColor = true;
            this.btnvolver.Click += new System.EventHandler(this.btnvolver_Click);
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(612, 71);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(75, 23);
            this.btnbuscar.TabIndex = 15;
            this.btnbuscar.Text = "Buscar";
            this.btnbuscar.UseVisualStyleBackColor = true;
            // 
            // lnkEliminar
            // 
            this.lnkEliminar.AutoSize = true;
            this.lnkEliminar.Location = new System.Drawing.Point(26, 214);
            this.lnkEliminar.Name = "lnkEliminar";
            this.lnkEliminar.Size = new System.Drawing.Size(55, 13);
            this.lnkEliminar.TabIndex = 16;
            this.lnkEliminar.TabStop = true;
            this.lnkEliminar.Text = "Eliminar (-)";
            this.lnkEliminar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEliminar_LinkClicked);
            // 
            // lnkNuevo
            // 
            this.lnkNuevo.AutoSize = true;
            this.lnkNuevo.Location = new System.Drawing.Point(985, 214);
            this.lnkNuevo.Name = "lnkNuevo";
            this.lnkNuevo.Size = new System.Drawing.Size(54, 13);
            this.lnkNuevo.TabIndex = 17;
            this.lnkNuevo.TabStop = true;
            this.lnkNuevo.Text = "Nuevo (+)";
            this.lnkNuevo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNuevo_LinkClicked);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.Width = 70;
            // 
            // RP
            // 
            this.RP.HeaderText = "RP";
            this.RP.Name = "RP";
            this.RP.ReadOnly = true;
            this.RP.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.RP.Width = 50;
            // 
            // UbicacionActual
            // 
            this.UbicacionActual.HeaderText = "Ubicación Actual";
            this.UbicacionActual.Name = "UbicacionActual";
            this.UbicacionActual.ReadOnly = true;
            // 
            // Propietario
            // 
            this.Propietario.HeaderText = "Propietario";
            this.Propietario.Name = "Propietario";
            this.Propietario.ReadOnly = true;
            // 
            // NombreAnimal
            // 
            this.NombreAnimal.HeaderText = "Nombre del Animal";
            this.NombreAnimal.Name = "NombreAnimal";
            this.NombreAnimal.ReadOnly = true;
            // 
            // Sexo
            // 
            this.Sexo.HeaderText = "Sexo";
            this.Sexo.Name = "Sexo";
            this.Sexo.ReadOnly = true;
            // 
            // FechaNacimiento
            // 
            this.FechaNacimiento.HeaderText = "Fecha de Nacimiento";
            this.FechaNacimiento.Name = "FechaNacimiento";
            this.FechaNacimiento.ReadOnly = true;
            // 
            // NoPartos
            // 
            this.NoPartos.HeaderText = "No de Partos";
            this.NoPartos.Name = "NoPartos";
            this.NoPartos.ReadOnly = true;
            this.NoPartos.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NoPartos.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.NoPartos.Width = 50;
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            this.Estado.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Estado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // UltimoPeso
            // 
            this.UltimoPeso.HeaderText = "Último Peso";
            this.UltimoPeso.Name = "UltimoPeso";
            this.UltimoPeso.ReadOnly = true;
            this.UltimoPeso.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UltimoPeso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // UltimoPlanSanitario
            // 
            this.UltimoPlanSanitario.HeaderText = "Último Plan Sanitario";
            this.UltimoPlanSanitario.Name = "UltimoPlanSanitario";
            this.UltimoPlanSanitario.ReadOnly = true;
            this.UltimoPlanSanitario.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UltimoPlanSanitario.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // BusquedaAnimales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 312);
            this.Controls.Add(this.lnkNuevo);
            this.Controls.Add(this.lnkEliminar);
            this.Controls.Add(this.btnbuscar);
            this.Controls.Add(this.dgvbusquedaanimal);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtcriterio);
            this.Controls.Add(this.btnvolver);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BusquedaAnimales";
            this.Text = "Búsqueda de Animales";
            this.Load += new System.EventHandler(this.BusquedaAnimales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvbusquedaanimal)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvbusquedaanimal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdubicacion;
        private System.Windows.Forms.RadioButton rdpropietario;
        private System.Windows.Forms.RadioButton rdnombre;
        private System.Windows.Forms.RadioButton rdrp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcriterio;
        private System.Windows.Forms.Button btnvolver;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.LinkLabel lnkEliminar;
        private System.Windows.Forms.LinkLabel lnkNuevo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewLinkColumn RP;
        private System.Windows.Forms.DataGridViewTextBoxColumn UbicacionActual;
        private System.Windows.Forms.DataGridViewTextBoxColumn Propietario;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreAnimal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sexo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaNacimiento;
        private System.Windows.Forms.DataGridViewLinkColumn NoPartos;
        private System.Windows.Forms.DataGridViewLinkColumn Estado;
        private System.Windows.Forms.DataGridViewLinkColumn UltimoPeso;
        private System.Windows.Forms.DataGridViewLinkColumn UltimoPlanSanitario;
    }
}