﻿namespace WindowsFormsApplication1
{
    partial class IngresoBajas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IngresoBajas));
            this.btndescartar = new System.Windows.Forms.Button();
            this.btnguardar = new System.Windows.Forms.Button();
            this.cmbtipo = new System.Windows.Forms.ComboBox();
            this.txtprecio = new System.Windows.Forms.TextBox();
            this.txtdetalle = new System.Windows.Forms.TextBox();
            this.txtpeso = new System.Windows.Forms.TextBox();
            this.txtrp = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fechaBaja = new System.Windows.Forms.Label();
            this.peso = new System.Windows.Forms.Label();
            this.rpBaja = new System.Windows.Forms.Label();
            this.dtfecha = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // btndescartar
            // 
            this.btndescartar.Location = new System.Drawing.Point(311, 257);
            this.btndescartar.Name = "btndescartar";
            this.btndescartar.Size = new System.Drawing.Size(92, 40);
            this.btndescartar.TabIndex = 63;
            this.btndescartar.Text = "Descartar";
            this.btndescartar.UseVisualStyleBackColor = true;
            this.btndescartar.Click += new System.EventHandler(this.btndescartar_Click);
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(173, 257);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(92, 40);
            this.btnguardar.TabIndex = 62;
            this.btnguardar.Text = "Guardar";
            this.btnguardar.UseVisualStyleBackColor = true;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // cmbtipo
            // 
            this.cmbtipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbtipo.FormattingEnabled = true;
            this.cmbtipo.Items.AddRange(new object[] {
            "Muerte",
            "Venta"});
            this.cmbtipo.Location = new System.Drawing.Point(397, 11);
            this.cmbtipo.Name = "cmbtipo";
            this.cmbtipo.Size = new System.Drawing.Size(121, 21);
            this.cmbtipo.TabIndex = 61;
            this.cmbtipo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // txtprecio
            // 
            this.txtprecio.Location = new System.Drawing.Point(397, 66);
            this.txtprecio.MaxLength = 5;
            this.txtprecio.Name = "txtprecio";
            this.txtprecio.Size = new System.Drawing.Size(100, 20);
            this.txtprecio.TabIndex = 60;
            this.txtprecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprecio_KeyPress);
            // 
            // txtdetalle
            // 
            this.txtdetalle.Location = new System.Drawing.Point(115, 172);
            this.txtdetalle.Name = "txtdetalle";
            this.txtdetalle.Size = new System.Drawing.Size(236, 20);
            this.txtdetalle.TabIndex = 59;
            this.txtdetalle.TextChanged += new System.EventHandler(this.txtdetalle_TextChanged);
            this.txtdetalle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdetalle_KeyPress);
            // 
            // txtpeso
            // 
            this.txtpeso.Location = new System.Drawing.Point(115, 62);
            this.txtpeso.MaxLength = 5;
            this.txtpeso.Name = "txtpeso";
            this.txtpeso.Size = new System.Drawing.Size(100, 20);
            this.txtpeso.TabIndex = 57;
            this.txtpeso.TextChanged += new System.EventHandler(this.txtpeso_TextChanged);
            this.txtpeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpeso_KeyPress);
            // 
            // txtrp
            // 
            this.txtrp.Location = new System.Drawing.Point(115, 12);
            this.txtrp.MaxLength = 3;
            this.txtrp.Name = "txtrp";
            this.txtrp.Size = new System.Drawing.Size(100, 20);
            this.txtrp.TabIndex = 56;
            this.txtrp.TextChanged += new System.EventHandler(this.txtrp_TextChanged);
            this.txtrp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrp_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(320, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 55;
            this.label6.Text = "Precio por lbs:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(320, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Tipo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Detalle";
            // 
            // fechaBaja
            // 
            this.fechaBaja.AutoSize = true;
            this.fechaBaja.Location = new System.Drawing.Point(17, 116);
            this.fechaBaja.Name = "fechaBaja";
            this.fechaBaja.Size = new System.Drawing.Size(76, 13);
            this.fechaBaja.TabIndex = 52;
            this.fechaBaja.Text = "Fecha de Baja";
            // 
            // peso
            // 
            this.peso.AutoSize = true;
            this.peso.Location = new System.Drawing.Point(17, 69);
            this.peso.Name = "peso";
            this.peso.Size = new System.Drawing.Size(56, 13);
            this.peso.TabIndex = 51;
            this.peso.Text = "Peso (lbs):";
            // 
            // rpBaja
            // 
            this.rpBaja.AutoSize = true;
            this.rpBaja.Location = new System.Drawing.Point(17, 19);
            this.rpBaja.Name = "rpBaja";
            this.rpBaja.Size = new System.Drawing.Size(25, 13);
            this.rpBaja.TabIndex = 50;
            this.rpBaja.Text = "RP:";
            // 
            // dtfecha
            // 
            this.dtfecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtfecha.Location = new System.Drawing.Point(115, 110);
            this.dtfecha.Name = "dtfecha";
            this.dtfecha.Size = new System.Drawing.Size(129, 20);
            this.dtfecha.TabIndex = 70;
            // 
            // IngresoBajas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 309);
            this.Controls.Add(this.dtfecha);
            this.Controls.Add(this.btndescartar);
            this.Controls.Add(this.btnguardar);
            this.Controls.Add(this.cmbtipo);
            this.Controls.Add(this.txtprecio);
            this.Controls.Add(this.txtdetalle);
            this.Controls.Add(this.txtpeso);
            this.Controls.Add(this.txtrp);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.fechaBaja);
            this.Controls.Add(this.peso);
            this.Controls.Add(this.rpBaja);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IngresoBajas";
            this.Text = "Ingreso Bajas";
            this.Load += new System.EventHandler(this.IngresoBajas_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btndescartar;
        private System.Windows.Forms.Button btnguardar;
        private System.Windows.Forms.ComboBox cmbtipo;
        private System.Windows.Forms.TextBox txtprecio;
        private System.Windows.Forms.TextBox txtdetalle;
        private System.Windows.Forms.TextBox txtpeso;
        private System.Windows.Forms.TextBox txtrp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label fechaBaja;
        private System.Windows.Forms.Label peso;
        private System.Windows.Forms.Label rpBaja;
        private System.Windows.Forms.DateTimePicker dtfecha;
    }
}