﻿namespace WindowsFormsApplication1
{
    partial class InformacionPlanesReproductivoPorVaca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InformacionPlanesReproductivoPorVaca));
            this.btnvolver = new System.Windows.Forms.Button();
            this.groupHistorial = new System.Windows.Forms.GroupBox();
            this.lnkNuevo = new System.Windows.Forms.LinkLabel();
            this.lnkEliminar = new System.Windows.Forms.LinkLabel();
            this.cmbanohasta = new System.Windows.Forms.ComboBox();
            this.cmbanodesde = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvplanrepro = new System.Windows.Forms.DataGridView();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IdPlan = new System.Windows.Forms.DataGridViewLinkColumn();
            this.RPToro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaPlan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AplicaDispositivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CondicionCorporal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comentario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoReproduccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstaPreñada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupInfo = new System.Windows.Forms.GroupBox();
            this.txtdiasgestacion = new System.Windows.Forms.TextBox();
            this.txtestaprenada = new System.Windows.Forms.TextBox();
            this.txtgestacion = new System.Windows.Forms.TextBox();
            this.txtnombreanimal = new System.Windows.Forms.TextBox();
            this.txtrp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupHistorial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvplanrepro)).BeginInit();
            this.groupInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnvolver
            // 
            this.btnvolver.Location = new System.Drawing.Point(331, 415);
            this.btnvolver.Name = "btnvolver";
            this.btnvolver.Size = new System.Drawing.Size(81, 36);
            this.btnvolver.TabIndex = 10;
            this.btnvolver.Text = "Volver";
            this.btnvolver.UseVisualStyleBackColor = true;
            this.btnvolver.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupHistorial
            // 
            this.groupHistorial.Controls.Add(this.lnkNuevo);
            this.groupHistorial.Controls.Add(this.lnkEliminar);
            this.groupHistorial.Controls.Add(this.cmbanohasta);
            this.groupHistorial.Controls.Add(this.cmbanodesde);
            this.groupHistorial.Controls.Add(this.label7);
            this.groupHistorial.Controls.Add(this.label6);
            this.groupHistorial.Controls.Add(this.dgvplanrepro);
            this.groupHistorial.Location = new System.Drawing.Point(12, 147);
            this.groupHistorial.Name = "groupHistorial";
            this.groupHistorial.Size = new System.Drawing.Size(724, 240);
            this.groupHistorial.TabIndex = 9;
            this.groupHistorial.TabStop = false;
            this.groupHistorial.Text = "Historial";
            // 
            // lnkNuevo
            // 
            this.lnkNuevo.AutoSize = true;
            this.lnkNuevo.Location = new System.Drawing.Point(664, 223);
            this.lnkNuevo.Name = "lnkNuevo";
            this.lnkNuevo.Size = new System.Drawing.Size(54, 13);
            this.lnkNuevo.TabIndex = 67;
            this.lnkNuevo.TabStop = true;
            this.lnkNuevo.Text = "Nuevo (+)";
            this.lnkNuevo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNuevo_LinkClicked);
            // 
            // lnkEliminar
            // 
            this.lnkEliminar.AutoSize = true;
            this.lnkEliminar.Location = new System.Drawing.Point(18, 223);
            this.lnkEliminar.Name = "lnkEliminar";
            this.lnkEliminar.Size = new System.Drawing.Size(55, 13);
            this.lnkEliminar.TabIndex = 66;
            this.lnkEliminar.TabStop = true;
            this.lnkEliminar.Text = "Eliminar (-)";
            this.lnkEliminar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEliminar_LinkClicked);
            // 
            // cmbanohasta
            // 
            this.cmbanohasta.FormattingEnabled = true;
            this.cmbanohasta.Location = new System.Drawing.Point(473, 39);
            this.cmbanohasta.Name = "cmbanohasta";
            this.cmbanohasta.Size = new System.Drawing.Size(121, 21);
            this.cmbanohasta.TabIndex = 65;
            // 
            // cmbanodesde
            // 
            this.cmbanodesde.FormattingEnabled = true;
            this.cmbanodesde.Location = new System.Drawing.Point(137, 34);
            this.cmbanodesde.Name = "cmbanodesde";
            this.cmbanodesde.Size = new System.Drawing.Size(121, 21);
            this.cmbanodesde.TabIndex = 64;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(406, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Año hasta:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Año Desde:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // dgvplanrepro
            // 
            this.dgvplanrepro.AllowUserToAddRows = false;
            this.dgvplanrepro.AllowUserToDeleteRows = false;
            this.dgvplanrepro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvplanrepro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.IdPlan,
            this.RPToro,
            this.FechaPlan,
            this.AplicaDispositivo,
            this.CondicionCorporal,
            this.Comentario,
            this.TipoReproduccion,
            this.EstaPreñada});
            this.dgvplanrepro.Location = new System.Drawing.Point(18, 86);
            this.dgvplanrepro.Name = "dgvplanrepro";
            this.dgvplanrepro.ReadOnly = true;
            this.dgvplanrepro.Size = new System.Drawing.Size(694, 130);
            this.dgvplanrepro.TabIndex = 0;
            this.dgvplanrepro.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.historialDataGridView_CellContentClick);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.ReadOnly = true;
            this.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IdPlan
            // 
            this.IdPlan.HeaderText = "Id Plan";
            this.IdPlan.Name = "IdPlan";
            this.IdPlan.ReadOnly = true;
            this.IdPlan.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IdPlan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // RPToro
            // 
            this.RPToro.HeaderText = "RP Toro";
            this.RPToro.Name = "RPToro";
            this.RPToro.ReadOnly = true;
            // 
            // FechaPlan
            // 
            this.FechaPlan.HeaderText = "Fecha Plan";
            this.FechaPlan.Name = "FechaPlan";
            this.FechaPlan.ReadOnly = true;
            // 
            // AplicaDispositivo
            // 
            this.AplicaDispositivo.HeaderText = "Aplica Dispositivo";
            this.AplicaDispositivo.Name = "AplicaDispositivo";
            this.AplicaDispositivo.ReadOnly = true;
            // 
            // CondicionCorporal
            // 
            this.CondicionCorporal.HeaderText = "Condición Corporal";
            this.CondicionCorporal.Name = "CondicionCorporal";
            this.CondicionCorporal.ReadOnly = true;
            // 
            // Comentario
            // 
            this.Comentario.HeaderText = "Comentario";
            this.Comentario.Name = "Comentario";
            this.Comentario.ReadOnly = true;
            // 
            // TipoReproduccion
            // 
            this.TipoReproduccion.HeaderText = "Tipo Reproducción";
            this.TipoReproduccion.Name = "TipoReproduccion";
            this.TipoReproduccion.ReadOnly = true;
            // 
            // EstaPreñada
            // 
            this.EstaPreñada.HeaderText = "¿Está Preñada?";
            this.EstaPreñada.Name = "EstaPreñada";
            this.EstaPreñada.ReadOnly = true;
            // 
            // groupInfo
            // 
            this.groupInfo.Controls.Add(this.txtdiasgestacion);
            this.groupInfo.Controls.Add(this.txtestaprenada);
            this.groupInfo.Controls.Add(this.txtgestacion);
            this.groupInfo.Controls.Add(this.txtnombreanimal);
            this.groupInfo.Controls.Add(this.txtrp);
            this.groupInfo.Controls.Add(this.label5);
            this.groupInfo.Controls.Add(this.label4);
            this.groupInfo.Controls.Add(this.label3);
            this.groupInfo.Controls.Add(this.label2);
            this.groupInfo.Controls.Add(this.label1);
            this.groupInfo.Location = new System.Drawing.Point(21, 12);
            this.groupInfo.Name = "groupInfo";
            this.groupInfo.Size = new System.Drawing.Size(619, 111);
            this.groupInfo.TabIndex = 8;
            this.groupInfo.TabStop = false;
            this.groupInfo.Text = "Información actual";
            // 
            // txtdiasgestacion
            // 
            this.txtdiasgestacion.Location = new System.Drawing.Point(464, 81);
            this.txtdiasgestacion.Name = "txtdiasgestacion";
            this.txtdiasgestacion.ReadOnly = true;
            this.txtdiasgestacion.Size = new System.Drawing.Size(100, 20);
            this.txtdiasgestacion.TabIndex = 9;
            // 
            // txtestaprenada
            // 
            this.txtestaprenada.Location = new System.Drawing.Point(128, 57);
            this.txtestaprenada.Name = "txtestaprenada";
            this.txtestaprenada.ReadOnly = true;
            this.txtestaprenada.Size = new System.Drawing.Size(100, 20);
            this.txtestaprenada.TabIndex = 8;
            // 
            // txtgestacion
            // 
            this.txtgestacion.Location = new System.Drawing.Point(128, 81);
            this.txtgestacion.Name = "txtgestacion";
            this.txtgestacion.ReadOnly = true;
            this.txtgestacion.Size = new System.Drawing.Size(100, 20);
            this.txtgestacion.TabIndex = 7;
            // 
            // txtnombreanimal
            // 
            this.txtnombreanimal.Location = new System.Drawing.Point(464, 37);
            this.txtnombreanimal.Name = "txtnombreanimal";
            this.txtnombreanimal.ReadOnly = true;
            this.txtnombreanimal.Size = new System.Drawing.Size(100, 20);
            this.txtnombreanimal.TabIndex = 6;
            // 
            // txtrp
            // 
            this.txtrp.Location = new System.Drawing.Point(128, 31);
            this.txtrp.Name = "txtrp";
            this.txtrp.ReadOnly = true;
            this.txtrp.Size = new System.Drawing.Size(100, 20);
            this.txtrp.TabIndex = 5;
            this.txtrp.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(316, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Días de gestación:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(316, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombre del animal:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Meses de gestación:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "¿Está preñada? :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "RP:";
            // 
            // InformacionPlanesReproductivoPorVaca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 463);
            this.Controls.Add(this.btnvolver);
            this.Controls.Add(this.groupHistorial);
            this.Controls.Add(this.groupInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InformacionPlanesReproductivoPorVaca";
            this.Text = "Informacion Planes Reproductivo Por Vaca";
            this.Load += new System.EventHandler(this.InformacionPlanesReproductivoPorVaca_Load);
            this.groupHistorial.ResumeLayout(false);
            this.groupHistorial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvplanrepro)).EndInit();
            this.groupInfo.ResumeLayout(false);
            this.groupInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnvolver;
        private System.Windows.Forms.GroupBox groupHistorial;
        private System.Windows.Forms.DataGridView dgvplanrepro;
        private System.Windows.Forms.GroupBox groupInfo;
        private System.Windows.Forms.TextBox txtdiasgestacion;
        private System.Windows.Forms.TextBox txtestaprenada;
        private System.Windows.Forms.TextBox txtgestacion;
        private System.Windows.Forms.TextBox txtnombreanimal;
        private System.Windows.Forms.TextBox txtrp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbanohasta;
        private System.Windows.Forms.ComboBox cmbanodesde;
        private System.Windows.Forms.LinkLabel lnkNuevo;
        private System.Windows.Forms.LinkLabel lnkEliminar;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewLinkColumn IdPlan;
        private System.Windows.Forms.DataGridViewTextBoxColumn RPToro;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaPlan;
        private System.Windows.Forms.DataGridViewTextBoxColumn AplicaDispositivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CondicionCorporal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comentario;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoReproduccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstaPreñada;
    }
}