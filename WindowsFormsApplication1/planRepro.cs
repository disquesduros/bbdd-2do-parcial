﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class planRepro
    {
        public string id_repro { get; set; }
        public string rp_padre { get; set; }
        public string rp_vaca { get; set; }
        public bool apli_dispo { get; set; }
        public float condi_corpo { get; set; }
        public bool palpacion { get; set; }
        public string comentario { get; set; }
        public string tipo_repro { get; set; }
        public planRepro() { }

        public planRepro(string id_repro, string rp_padre, string rp_vaca, bool apli_dispo, float condi_corpo,bool palpacion, string comentario, string tipo_repro)
        {
            this.id_repro = id_repro;
            this.rp_vaca = rp_vaca;
            this.rp_padre = rp_padre;
            this.apli_dispo = apli_dispo;
            this.condi_corpo = condi_corpo;
            this.palpacion = palpacion;
            this.comentario = comentario;
            this.tipo_repro = tipo_repro;
        }
    }
}
