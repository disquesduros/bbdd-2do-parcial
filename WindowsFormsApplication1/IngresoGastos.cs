﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoGastos : Form
    {
        public IngresoGastos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCantidad.Text) || string.IsNullOrWhiteSpace(txtCostoU.Text) || string.IsNullOrWhiteSpace(txtDescuento.Text) || string.IsNullOrWhiteSpace(dateTimePicker1.Text))
                //si el tipo de gasto es personal hacer que la cedula sea obligatoria
                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (comboBox1.Text.Equals("PERSONAL"))
            {
                if (string.IsNullOrWhiteSpace(txtCedula.Text))
                {
                    MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if (txtCedula.Text.Trim(' ').Length!=10) { 
                    MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo Cedula (deben ser 10 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }


        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private static void SoloDecimales(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtCostoU_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e,false);
        }

        private void txtCargos_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e, false);
        }

        private void txtDescuento_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e, false);
        }

        private void txtCedula_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
