﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class BusquedaAnimales : Form
    {
        public BusquedaAnimales()
        {
            InitializeComponent();
        }

        private void lnkNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoAnimalNuevo animal = new WindowsFormsApplication1.IngresoAnimalNuevo();
            animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            animal.Show();
            this.Hide();
        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void dgvbusquedaanimal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                WindowsFormsApplication1.ConsultaInfoAnimal animal = new WindowsFormsApplication1.ConsultaInfoAnimal();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            } else if (e.ColumnIndex == 7)
            {
                WindowsFormsApplication1.ConsInfoParto animal = new WindowsFormsApplication1.ConsInfoParto();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }

            else if (e.ColumnIndex == 8)
            {
                InformacionPlanesReproductivoPorVaca animal = new InformacionPlanesReproductivoPorVaca();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }
            else if (e.ColumnIndex == 9)
            {
                WindowsFormsApplication1.ConsultaInfoPesajeporAnimal animal = new WindowsFormsApplication1.ConsultaInfoPesajeporAnimal();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }
            else if (e.ColumnIndex == 10)
            {
                WindowsFormsApplication1.ConsultaInfoPlanSanitario animal = new WindowsFormsApplication1.ConsultaInfoPlanSanitario();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.Show();
                this.Hide();
            }            
        }

        private void BusquedaAnimales_Load(object sender, EventArgs e)
        {
           /* MySqlCommand cmd = new MySqlCommand("buscar_animal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dgvbusquedaanimal.Rows.Add(true, reader.GetString(1), reader.GetString(4), reader.GetString(5), reader.GetString(2), reader.GetString(3));*/
        }
<<<<<<< HEAD

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            dgvbusquedaanimal.Rows.Clear();
            string criterio = txtcriterio.Text.Trim();
            MySqlCommand cmd = null;

            if (criterio == "")
            {
                cmd = new MySqlCommand("buscar_animal", coneccionBase.ObtenerConexion());
            }
            else if (rdrp.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_rp", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdnombre.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_nombre", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdpropietario.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_propietario", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdubicacion.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_area", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else cmd = new MySqlCommand("buscar_animal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dgvbusquedaanimal.Rows.Add(false, reader.GetString(0), reader.GetString(2), reader.GetString(7), reader.GetString(3), reader.GetString(4));
        }

        private void lnkEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvbusquedaanimal.SelectedRows.Count == 1)
            {
                string inRp = Convert.ToString(dgvbusquedaanimal.CurrentRow.Cells[1].Value);
                animalAcciones.Eliminar(inRp);
                dgvbusquedaanimal.Rows.Remove(dgvbusquedaanimal.CurrentRow);

            }
            else
                MessageBox.Show("Debe seleccionar una fila");
        }
=======
>>>>>>> parent of 620ed6f... busqueda animales listo
    }
}
